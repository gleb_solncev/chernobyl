@extends('layouts.user')

@section('content-page')

 <div class="card">
     <div class="card-header">
         Персональные данные
     </div>
     <div class="card-body">
         {{ Form::model($user, ['route' => 'user.update']) }}
         @csrf
         <div class="form-group">
             <label class="w-100">
                 Имя:
                 {{ Form::text('name', null, ['class' => 'form-control']) }}
             </label>
         </div>
         <div class="form-group">
             <label class="w-100">
                 Номер телефона:
                 {{ Form::text('phone', null, ['class' => 'form-control']) }}
             </label>
         </div>
         <div class="form-group">
             <label class="w-100">
                 Почта:
                 {{ Form::text('email', null, ['class' => 'form-control']) }}
             </label>
         </div>
         <button type="submit" class="btn btn-primary">Сохранить</button>
         {{ Form::close() }}
     </div>
 </div>

@endsection
