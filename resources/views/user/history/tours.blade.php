@extends('layouts.user')

@section('content-page')
    @php /** @var $orders \Illuminate\Support\Collection */  @endphp
    @php /** @var $tour \Modules\Tour\Entities\Tour */ @endphp
    @php /** @var $product \Modules\Admin\Entities\Checkout\Product */ @endphp

    <div class="card">
        <div class="card-header">
            Персональные данные
        </div>
        <div class="card-body">
            {{ Form::model(null, ['route' => 'user.update']) }}
            @csrf

            @foreach($orders as $order)
                @php $product = $order->product @endphp
                @php $tour = $product->tour @endphp
                @php $total = ($product->q_ukraine * $product->price_ukr) + ($product->q_other * $product->price_oth) @endphp


                # {{$tour->id}} n
                {{ $tour->firstByName('title') }}
                Количество: {{ array_sum($product->only(['q_ukraine', 'q_other'])) }}
                Итого: {{ $total }} {{ $product->currency }}

                @if($order->options)
                    @foreach($order->options as $option)
                        <br>
                        {{ $option->entity->firstByName('title')->value }}
                        Кол-во: {{ $option->quantity }}
                        Цена: {{ $option->price }} {{ $option->currency }}
                    @endforeach
                    <br>
                @endif
                Осталось : {{ $total - ($total * $order->payment->method)/100 }}
            @endforeach

            <button type="submit" class="btn btn-primary">Сохранить</button>
            {{ Form::close() }}
        </div>
    </div>

@endsection
