@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">MENU</div>
                    <div class="card-body">

                        {!! Menu::get('UserMenu')->asUl() !!}
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                @yield('content-page')
            </div>
        </div>
    </div>
@endsection
