<?php

return [
    'role'=> [
        'admin' => 'Администратор',
        'user' => 'Пользователь',
        'disabled' => 'Заблокирован',
    ],
    'forms' => [
        'status' => [
            'unconfirmed' => 'Не подтвержден',
            'approval' => 'На утверждении',
            'confirmed' => 'Подтвержден',
        ]
    ],
    'modules' => [
        'admin' => [
            'title' => 'Админ. панель',
            'description' => 'Модуль который отвечает за админ часть'
        ],
        'option' => [
            'title' => 'Опции',
            'description' => 'Опции для туров и для сувениров'
        ],
        'tour' => [
            'title' => 'Туры',
            'description' => 'Оформление туров, также в меню пользователя'
        ],
        'user' => [
            'title' => 'Пользователи',
            'description' => 'Пользователи, создание радктировани и удаление'
        ],
        'locale' => [
            'title' => 'Языки',
            'description' => 'Языки с которыми работаем: '.implode(config('app.currencies'), ', ')
        ],
        'media' => [
            'title' => 'Медиафайлы',
            'description' => 'Нужно для хранения изображений',
        ],
        'souvenir' => [
            'title' => 'Суверины',
            'description' => 'Интернет магазин для сувенирной продукции',
        ],
    ],
    'payment' => [
        'status' => [
            'done' => 'Оплачен'
        ]
    ],
    'order' => [
        'status' => [
            'expect' => 'Ожидание',
            'cancel' => 'Закрыт',
            'done' => 'Подтвержден'
        ],
        'type' => [
            'tour' => 'Тур',
            'souvenirs' => 'Сувенир',
        ]
    ]
];
