<?php

return [
    'role'=> [
        'admin' => 'Администратор',
        'user' => 'Пользователь',
        'disabled' => 'Заблокирован',
    ],
    'forms' => [
        'status' => [
            'unconfirmed' => 'Не подтвержден',
            'approval' => 'На утверждении',
            'confirmed' => 'Подтвержден',
        ]
    ],
    'modules' => [
        'admin' => [
            'title' => 'Админ. панель',
            'description' => 'Модуль который отвечает за админ часть'
        ],
        'option' => [
            'title' => 'Опции',
            'description' => 'Опции для туров'
        ],
        'tour' => [
            'title' => 'Туры',
            'description' => 'Туры для пользователя и в админке'
        ],
        'user' => [
            'title' => 'Пользователи',
            'description' => 'Пользователи, создание радктировани и удаление'
        ],
        'locale' => [
            'title' => 'Языки',
            'description' => strtoupper(implode(config('app.locales'), ', '))
        ],
        'media' => [
            'title' => 'Медиафайлы',
            'description' => 'Нужно для хранения изображений',
        ],
        'souvenir' => [
            'title' => 'Суверины',
            'description' => 'Интернет магазин для сувенирной продукции. (Вшиты отдельные опции)',
        ],
        'currency' => [
            'title' => 'Курс валют',
            'description' => 'Для сувенирной продукции. Где цена указывается для определенной валюты.',
        ],
    ],
    'payment' => [
        'status' => [
            'done' => 'Оплачен'
        ]
    ],
    'order' => [
        'status' => [
            'expect' => 'Ожидание',
            'cancel' => 'Закрыт',
            'done' => 'Подтвержден'
        ],
        'type' => [
            'tour' => 'Тур',
            'souvenirs' => 'Сувенир',
        ]
    ]
];
