<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Nwidart\Modules\Facades\Module;

class UserMenuMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \Menu::make('UserMenu', function ($menu) {
            $user = Auth::user();

            if(optional($user)->role == "admin")
                $menu->add('Админ панель', ['route' => 'admin.home']);

            if(Module::find('tour')->isEnabled())
                $menu->add('Мои поездки', ['route' => 'user.tours']);
            $menu->add('Персональные данные', ['route' => 'user.home']);
            $menu->add('История заказов в Магазине', ['route' => 'user.shop']);

        });
        return $next($request);
    }
}
