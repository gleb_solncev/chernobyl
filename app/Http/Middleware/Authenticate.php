<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Nwidart\Modules\Facades\Module;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if(Module::find('user')->isDisabled()){
            return '/';
        }

        if (! $request->expectsJson()) {
            return route('login');
        }
    }
}
