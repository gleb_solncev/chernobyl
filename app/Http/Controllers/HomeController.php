<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Entities\Checkout\FormUser;
use Modules\Tour\Entities\Tour;
use RealRashid\SweetAlert\Facades\Alert;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();

        return view('user.home',
            compact('user')
        );
    }

    public function update(Request $request)
    {
        $user = Auth::user();
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->save();

        Alert::success('Персональные данные', "Обновлены");
        return redirect()->back();
    }

    public function tours(){
        $user = Auth::user();
        $orders = $user->orders->where('type', 'tour');

        return view('user.history.tours',
            compact('orders')
        );

    }
    public function shop(){

    }



    public function user_form($share_code){
        $form = FormUser::where(compact('share_code'))->first();
        if(!$form) return abort(404);
        if($form->passport and $form->invoice and $form->phone) return dd('Уже заполнен!');

        dd($form);
    }
}
