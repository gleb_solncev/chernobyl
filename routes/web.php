<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['prefix' => App\Http\Middleware\LocaleMiddleware::getLocale()], function() {

    Route::get('/', function () {
        return view('welcome');
    });


    Route::group([
        'prefix' => 'me',
        'as' => 'user.'
    ], function () {
        Route::get('/home', 'HomeController@index')->name('home');
        Route::post('/update', 'HomeController@update')->name('update');

        Route::get('/tours', 'HomeController@tours')->name('tours');
        Route::get('/shop', 'HomeController@shop')->name('shop');
    });

    Route::get('/share/{slug}/link', 'HomeController@user_form')->name('share_link');
});
