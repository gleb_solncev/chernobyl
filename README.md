<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## INSTALL

- [composer install](https://getcomposer.org/)
- .env.example - .env
- php artisan key:generate
- php artisan config:cache
- php artisan migrate:fresh -seed
- php artisan module:list 
    - По этому списку нужно сделать сиды:
    - php artisan module:seed Tour
    - php artisan module:seed Media
    - php artisan module:seed Souvenir
    - php artisan module:seed Option
    - php artisan module:seed Currency
    - php artisan module:seed Admin
- REMOVE pulblic/storage
- php artisan storage:link

Admin: admin@admin.com|password
    
## DOCUMENTATIONS

- [Laravel 6](https://laravel.com/docs/6.x)
- [SwetAlert2](https://sweetalert2.github.io/)
- [Laravel-Modules](https://nwidart.com/laravel-modules/v6/introduction)

## TODO
 
- [ ] Вывод верстки\фронта (BOOTSTRAP)
- [ ] Модернизация Закакзов: Тур, Сувениры.
- [ ] Определиться какой текст является статичным.
- [X] Админ панель
    - [x] Пользователи
    - [x] Курс валют
    - [x] Модули
    - [x] Туры
    - [x] Сувениры
    - [x] Заказы
    - [x] Опцци Тура
    - [x] Опцци Сувениров
    - [x] Мультиязычность
    - [x] Учет количества заказов\Пользователей
- [ ] Способы оплаты
    - [ ] WayForPay
    - [ ] LiqPay
    - [ ] PayPal
- [ ] Разрешительные формы
    - [ ] Учет количества граждан нужной национальности
    - [ ] Возможность добавлять формы
    - [ ] Двухэтапное подтверждение формы
     
