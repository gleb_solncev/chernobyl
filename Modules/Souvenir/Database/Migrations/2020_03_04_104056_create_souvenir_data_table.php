<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSouvenirDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('souvenir_data', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('title');
            $table->string('name');
            $table->text('value');
            $table->enum('lang', config('app.locales'));
            $table->string('field_type');

            $table->unsignedBigInteger('souvenir_id');
            $table->foreign('souvenir_id')->references('id')->on('souvenirs')->onDelete('CASCADE');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('souvenir_data');
    }
}
