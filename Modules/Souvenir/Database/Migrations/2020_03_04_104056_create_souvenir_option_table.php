<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSouvenirOptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('souvenir_options', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->boolean('selection');

            $table->string('attr_title');
            $table->string('attr_name');
            $table->string('opti_title');
            $table->string('opti_name');

            $table->unsignedBigInteger('souvenir_id');
            $table->foreign('souvenir_id')->references('id')->on('souvenirs')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('souvenir_options');
    }
}
