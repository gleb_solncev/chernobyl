<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CraeteSouvenirsPriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('souvenir_price', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('currency');
            $table->string('value');

            $table->unsignedBigInteger('souvenir_id');
            $table->foreign('souvenir_id')->references('id')->on('souvenirs')->onDelete('CASCADE');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
