<?php

namespace Modules\Souvenir\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Media\Entities\Media;

class SouvenirsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('souvenirs')->insert([
            [
                'id' => 1,
                'slug' => 'footbolka-black',
                'img' => Media::first()->id,
            ]
        ]);

        DB::table('souvenir_price')->insert([
            [
                'currency' => 'uah',
                'value' => 1000,
                'souvenir_id' => 1,
            ],
        ]);

        DB::table('souvenir_data')->insert([
            [
                'title' => 'Название товара',
                'name' => 'title',
                'lang' => 'ru',
                'value' => 'Футблока Радиоктивный волк чернобыля',
                'field_type' => 'input',
                'souvenir_id' => 1,
            ],
            [
                'title' => 'Название товара',
                'name' => 'title',
                'lang' => 'en',
                'value' => 'Футблока Радиоктивный волк чернобыля',
                'field_type' => 'input',
                'souvenir_id' => 1,
            ],
            [
                'title' => 'Название товара',
                'name' => 'title',
                'lang' => 'uk',
                'value' => 'Футблока Радиоктивный волк чернобыля',
                'field_type' => 'input',
                'souvenir_id' => 1,
            ],
            //////
            [
                'title' => 'Описание товара',
                'name' => 'description',
                'lang' => 'ru',
                'value' => 'Описание: Для флекса',
                'field_type' => 'input',
                'souvenir_id' => 1,
            ],
            [
                'title' => 'Описание товара',
                'name' => 'description',
                'lang' => 'en',
                'value' => 'Описание: Для флекса',
                'field_type' => 'input',
                'souvenir_id' => 1,
            ],
            [
                'title' => 'Описание товара',
                'name' => 'description',
                'lang' => 'uk',
                'value' => 'Описание: Для флекса',
                'field_type' => 'input',
                'souvenir_id' => 1,
            ],
        ]);



        DB::table('souvenir_options')->insert([
            [
                'selection' => false,
                'attr_name' => 'color',
                'attr_title' => 'Цвет',
                'opti_name' => 'black',
                'opti_title' => 'Черный',
                'souvenir_id' => 1,
                'created_at' => now()
            ],
            [
                'selection' => true,
                'attr_name' => 'size',
                'attr_title' => 'Размер',
                'optir_name' => 'S',
                'optir_title' => 'S',
                'souvenir_id' => 1,
                'created_at' => now()
            ],
            [
                'selection' => true,
                'attr_name' => 'size',
                'attr_title' => 'Размер',
                'optir_name' => 'L',
                'optir_title' => 'L',
                'souvenir_id' => 1,
                'created_at' => now()
            ],
            [
                'selection' => true,
                'attr_name' => 'size',
                'attr_title' => 'Размер',
                'optir_name' => 'XL',
                'optir_title' => 'XL',
                'souvenir_id' => 1,
                'created_at' => now()
            ],
        ]);









    }
}
