<?php

namespace Modules\Souvenir\Entities;

use App\Http\Middleware\LocaleMiddleware;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Session;
use Modules\Currency\Entities\Currency;
use Modules\Currency\Http\Controllers\CurrencyController;

class Souvenir extends Model
{
    use SoftDeletes;

    protected $fillable = ['slug', 'img'];
    protected $table = 'souvenirs';

    public function media()
    {
        return $this->belongsTo('Modules\Media\Entities\Media', 'img', 'id');
    }

    public function data()
    {
        return $this->hasMany('Modules\Souvenir\Entities\Souvenir\Data', 'souvenir_id', 'id');
    }

    public function fieldsByLocale()
    {
        $lang = LocaleMiddleware::getLocale()?:LocaleMiddleware::$mainLanguage;
        return $this->data()->where(compact('lang'))->get();
    }

    public function getField($name, $lang=null)
    {
        $lang = $lang?:LocaleMiddleware::getLocale()?:LocaleMiddleware::$mainLanguage;
        return $this->data()->where(compact('name', 'lang'))->first();
    }

    public function options(){
        return $this->hasMany('Modules\Souvenir\Entities\Souvenir\Option', 'souvenir_id', 'id');
    }
    public function price(){
        return $this->hasOne('Modules\Souvenir\Entities\Souvenir\Price', 'souvenir_id', 'id');
    }

    public function priceWithCurrency(){
        $item = $this->price()->first();
        $currency = Currency::where('name', CurrencyController::getCurrency())->first();
        $priceCurrency = Currency::where('name', $item->currency)->first();

        return round($item->value/$priceCurrency->coefficient * $currency->coefficient, 1)." ".strtoupper($currency->name);
    }
}


