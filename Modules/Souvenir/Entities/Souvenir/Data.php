<?php

namespace Modules\Souvenir\Entities\Souvenir;

use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    protected $fillable = ['field_type', 'value', 'name', 'title', 'lang'];
    protected $table = 'souvenir_data';

}
