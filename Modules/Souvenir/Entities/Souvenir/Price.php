<?php

namespace Modules\Souvenir\Entities\Souvenir;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $fillable = ['currency', 'value'];
    protected $table = 'souvenir_price';
}
