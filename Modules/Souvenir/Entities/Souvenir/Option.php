<?php

namespace Modules\Souvenir\Entities\Souvenir;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $fillable = ['attr_name', 'attr_title', 'opti_title', 'opti_name', 'selection'];
    protected $table = 'souvenir_options';
}
