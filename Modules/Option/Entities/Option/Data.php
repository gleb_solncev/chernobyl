<?php

namespace Modules\Option\Entities\Option;

use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    protected $fillable = ['value', 'field_type', 'lang', 'title', 'name'];
    protected $table = 'option_data';
}
