<?php

namespace Modules\Option\Entities\Option;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $fillable = ['value', 'currency'];
    protected $table = 'option_price';
}
