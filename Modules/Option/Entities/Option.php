<?php

namespace Modules\Option\Entities;

use App\Http\Middleware\LocaleMiddleware;
use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $fillable = ['name', 'image'];
    protected $table = 'options';

    public function data(){
        return $this->hasMany('Modules\Option\Entities\Option\Data', 'option_id', 'id');
    }
    public function dataWithLocale($lang=null){
        return $this->hasMany('Modules\Option\Entities\Option\Data', 'option_id', 'id')
            ->where('lang', $lang?:(LocaleMiddleware::getLocale()?:LocaleMiddleware::$mainLanguage));
    }

    public function firstByName($name){
        return $this->hasMany('Modules\Option\Entities\Option\Data', 'option_id', 'id')
            ->where('lang', LocaleMiddleware::getLocale()?:LocaleMiddleware::$mainLanguage)
            ->where('name', $name)
            ->first();
    }

    public function price(){
        return $this->hasMany('Modules\Option\Entities\Option\Price', 'option_id', 'id');
    }

    public function tours(){
        return $this->belongsToMany('Modules\Tour\Entities\Tour', 'option_tours');
    }

    public function media(){
        return $this->hasOne('Modules\Media\Entities\Media', 'id', 'image');
    }
}
