<?php

namespace Modules\Option\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OptionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('options')->insert([
            [
                'id' => 1,
                'name' => 'dozimetr',
                'image' => 'https://images.ua.prom.st/117129737_w640_h640_mks-05-terra-p-dozimetr-radiometr.jpg',
            ]
        ]);

        DB::table('option_price')->insert([
            [
                'value' => '100',
                'currency' => 'uah',
                'option_id' => 1,
            ],
            [
                'value' => '20',
                'currency' => 'usd',
                'option_id' => 1,
            ],
            [
                'value' => '15',
                'currency' => 'eur',
                'option_id' => 1,
            ],
        ]);

        DB::table('option_data')->insert([
            [
                'name' => 'title',
                'title' => 'Заголовок',
                'field_type' => 'input',
                'value' => 'Дозиметр',
                'lang' => 'ru',
                'option_id' => 1,
            ],
            [
                'name' => 'title',
                'title' => 'Заголовок',
                'field_type' => 'input',
                'value' => 'Дозиметр[en]',
                'lang' => 'en',
                'option_id' => 1,
            ],
            [
                'name' => 'title',
                'title' => 'Заголовок',
                'field_type' => 'input',
                'value' => 'Дозиметр[uk]',
                'lang' => 'uk',
                'option_id' => 1,
            ],
        ]);

        DB::table('option_tours')->insert([
            [
                'tour_id' => 1,
                'option_id' => 1,
            ],
        ]);




    }
}
