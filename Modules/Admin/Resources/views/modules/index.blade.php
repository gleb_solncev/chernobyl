@extends('admin::layouts.master')

@section('content')
    <div class="card">
        <div class="card-header">HEADER</div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Название</th>
                            <th>Описание</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        @foreach($list as $num => $item)
                            <tbody>
                            <tr>
                                <td>
                                    {{ ++$num }}
                                </td>
                                <td>{{ __($item['title']) }}</td>
                                <td>{{ __($item['description']) }}</td>
                                <td class="d-flex justify-content-start">
                                    {!! Form::open(['route' => 'admin.modules.store', 'method' => 'POST']) !!}
                                        @if($item['enable'])
                                            <button class="btn btn-danger" name="{{ $item['name'] }}" value="off" {{ in_array($item['name'], ["user", 'admin'])?"disabled":null }}>Выкл</button>
                                        @else
                                            <button class="btn btn-success" name="{{ $item['name'] }}" value="on" {{ in_array($item['name'], ["user", 'admin'])?"disabled":null }}>Вкл</button>
                                        @endif
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
