@extends('admin::layouts.master')

@section('content')
    <div class="row mb-2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    {!! Form::open(['route' => ['admin.media.store'], 'method' => 'POST', 'files' => true]) !!}
                        <div class="d-flex justify-content-between">
                            {!! Form::file('media', ['class' => 'form-control', 'style' => 'padding: 3px;']) !!}
                            <button class="btn btn-success" type="submit">Добавить</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Медиафайлы</div>
                <div class="card-body">
                    <div class="row">
                        @foreach($media as $image)
                            <div class="col-md-3 mb-2">
                                <img src="{{ $image->src }}" style="width: 100%;height: auto;">
                                <div style="position: absolute;top: 10px;left: 70%;">
                                    {!! Form::open(['route' => ['admin.media.destroy', $image->id], 'method' => 'DELETE']) !!}
                                    <button class="btn btn-danger" type="submit">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
