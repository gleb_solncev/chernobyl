<div class="parent-ajax-modal-box">
    <div class="w-25 mb-2 mt-2">
        <img src="{{ isset($src)?$src:null }}" class="w-100 h-auto image-ajax">
    </div>
<!-- Button trigger modal -->
    <button type="button" class="btn btn-primary ajax-loader-media" data-toggle="modal" data-target="#exampleModal">
        Выбрать изображение
    </button>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="ajax-images">
                        <div class="row images">
                            Загрузка
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary close-image" data-dismiss="modal">Закрыть</button>
                    <button type="button" class="btn btn-primary save-image">Выбрал</button>
                </div>
            </div>
        </div>
    </div>
</div>

@section('media')
    <script>
        $(function () {
            $BODY = $('.parent-ajax-modal-box');
            var my_inp = $("input[name='{{ $localName }}']");

            $BODY.on('click', '.ajax-loader-media', function (e) {
                e.preventDefault();

                $.post('{{ route("admin.ajax-media") }}', {"_token": '{{ csrf_token() }}'}, function (responce) {
                    var row = $BODY.find('.images');
                    row.html('');

                    for (i = 0; i < responce.length; i++) {
                        var item = responce[i];
                        row.append(
                            '<div class="col-md-3">\n' +
                            '    <label>' +
                            '        <img src="' + item.src + '" class="w-100 h-auto">\n' +
                            '        <input type="radio" name="id" value="' + item.id + '">' +
                            '    </label>' +
                            '</div>'
                        );
                    }

                    $chec_inp = my_inp.val();
                    $('.images input[name="id"][value="' + $chec_inp + '"]').prop('checked', true);
                })
            });

            $BODY.on('click', '.save-image', function (e) {
                e.preventDefault();
                var id = $('.images input[name="id"]:checked');
                var image = id.parent().find('img');
                $('.image-ajax').attr('src', image.attr('src'));

                my_inp.val(id.val());
                $('.close-image').click();
            });
        })
    </script>
@endsection
