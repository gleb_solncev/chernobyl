@extends('admin::layouts.master')

@section('content')
    <div class="row mb-4">
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                    Админ панель
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Статистика заказов</div>
                <div class="card-body">
                    <ul>
                        <li>За год: {{ $stat_orders->get('year') }}</li>
                        <li>За месяц: {{ $stat_orders->get('month') }}</li>
                        <li>За сегодня: {{ $stat_orders->get('day') }}</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Статистика пользователей</div>
                <div class="card-body">
                    <ul>
                        <li>За год: {{ $stat_users->get('year') }}</li>
                        <li>За месяц: {{ $stat_users->get('month') }}</li>
                        <li>За сегодня: {{ $stat_users->get('day') }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
