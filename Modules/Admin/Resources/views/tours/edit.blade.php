@extends('admin::layouts.master')

@section('content')
    <div class="card">
        <div class="card-header">CREATE</div>
        <div class="card-body">

            {!! Form::model($item, ['route' => ['admin.tours.update', $item->id], 'method' => 'PATCH']) !!}

            <div class="row">
                <div class="col-md-12 d-flex justify-content-end">

                    <a style="color:white" class="btn btn-secondary mr-2" onclick="location.href='{{ route("admin.users.index") }}'">Вернуться</a>
                    <button class="btn btn-primary">Обновить</button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @include('admin::tours.includes.form')
                </div>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection

