
<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-data" role="tab"
           aria-controls="nav-home" aria-selected="true">
            Контент
        </a>
        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-prices" role="tab"
           aria-controls="nav-profile" aria-selected="false">
            Цены
        </a>
        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-dates" role="tab"
           aria-controls="nav-profile" aria-selected="false">
            Даты
        </a>
    </div>
</nav>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-data" role="tabpanel" aria-labelledby="nav-home-tab">
        <div class="row">
            <div class="col-md-12">
                @foreach($item->data as $data)
                    @if($data->type_field == "input")
                        <div class="form-group">
                            <label class="w-100">
                                {{ $data->title }}:
                                {{ Form::text('data['.$data->name.'][value]', null, ['class' => 'form-control']) }}
                            </label>
                        </div>
                    @elseif($data->type_field == "media")
                        <div class="form-group">
                            <label class="w-100">
                                {{ $data->title }}:
                                @include('admin::media.includes.modal', ['localName' => 'data['.$data->name.'][value]', 'src' => optional($item->media)->src?:"/not-found-photo.jpg"])
                                {{ Form::hidden('data['.$data->name.'][value]') }}
                            </label>
                        </div>
                    @else
                        {{ $item->type_field }}
                    @endif
                @endforeach
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="nav-prices" role="tabpanel" aria-labelledby="nav-profile-tab">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="w-100">
                        Цена UAH для Украинцев:
                        {{ Form::number('price[ukraine][uah][value]', null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="w-100">
                        Цена USD для Украинцев:
                        {{ Form::number('price[ukraine][usd][value]', null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="w-100">
                        Цена EUR для Украинцев:
                        {{ Form::number('price[ukraine][eur][value]', null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="w-100">
                        Цена UAH для Иностранцев:
                        {{ Form::number('price[other][uah][value]', null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="w-100">
                        Цена USD для Иностранцев:
                        {{ Form::number('price[other][usd][value]', null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="w-100">
                        Цена EUR для Иностранцев:
                        {{ Form::number('price[other][eur][value]', null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="nav-dates" role="tabpanel" aria-labelledby="nav-profile-tab">
        <div class="repeat">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="w-100">
                            Добавить:
                            <button class="btn btn-primary form-control add-new-date" type="button">+</button>
                        </label>
                    </div>
                </div>
            </div>
            @foreach($item->date as $num => $date)
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="w-100">
                                дата начало:
                                {{ Form::date('date['.$num.'][start]', null, ['class' => 'form-control']) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="w-100">
                                дата конец:
                                {{ Form::date('date['.$num.'][end]', null, ['class' => 'form-control']) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="w-100">
                                Удалить:
                                <button type="button" class="btn btn-danger form-control rm-data">X</button>
                            </label>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>



@section('scripts')
    @yield('media')
    <script>
        jQuery(function(){
            html_code = '<div class="row">\n' +
                '                <div class="col-md-5">\n' +
                '                    <div class="form-group">\n' +
                '                        <label class="w-100">\n' +
                '                            дата начало:\n' +
                '                            <input type="date" name="date[0][start]" class="form-control">\n' +
                '                        </label>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '                <div class="col-md-5">\n' +
                '                    <div class="form-group">\n' +
                '                        <label class="w-100">\n' +
                '                            дата конец:\n' +
                '                            <input type="date" name="date[0][end]" class="form-control">\n' +
                '                        </label>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '                <div class="col-md-2">\n' +
                '                    <div class="form-group">\n' +
                '                        <label class="w-100">\n' +
                '                            Удалить:\n' +
                '                            <button type="button" class="btn btn-danger form-control rm-data">X</button>\n' +
                '                        </label>\n' +
                '                    </div>\n' +
                '                </div>'+
                '            </div>';

            $('.add-new-date').click(function(){
                length = $('.repeat').find('.rm-data').length;
                $(html_code).clone()
                    .find('input[name="date[0][start]"]').attr('name', 'date['+length+'][start]')
                    .parents('.row')
                    .find('input[name="date[0][end]"]').attr('name', 'date['+length+'][end]')
                    .parents('.row')
                    .appendTo('.repeat');
            });

            $('html').on('click', '.rm-data', function(){
                $(this).parent().parent().parent().parent().remove();
            });
        })
    </script>
@endsection
