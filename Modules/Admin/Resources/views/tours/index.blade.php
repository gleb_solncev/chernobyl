@extends('admin::layouts.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 d-flex justify-content-end">
                            <button class="btn btn-success"
                                    onclick="location.href='{{ route('admin.tours.create') }}'">
                                Создать
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Туры</div>
                <div class="card-body">
                    @if($list->isEmpty())
                        <div class="row">
                            <div class="col-md-12">
                                Здесь пусто....
                            </div>
                        </div>
                    @else
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-hover hover-pointer">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Название</th>
                                        <th>Цены</th>
                                        <th>Создан</th>
                                        <th>Действия</th>
                                    </tr>
                                    </thead>
                                    @foreach($list as $item)
                                        <tbody>
                                        <tr onclick='location.href="{{ route('admin.tours.edit', $item->id) }}"'>
                                            <td class="align-middle">
                                                <a href="{{ route('admin.tours.edit', $item->id) }}">
                                                    {{ $item->id }}
                                                </a>
                                            </td>
                                            <td class="align-middle">{{ $item->firstByName('title') }}</td>
                                            <td class="align-middle">
                                                USD: {{ $item->getPriceByCurrency('USD')->pluck('value')->implode('/') }}
                                                <br>
                                                UAH: {{ $item->getPriceByCurrency('UAH')->pluck('value')->implode('/') }}
                                                <br>
                                                EUR: {{ $item->getPriceByCurrency('EUR')->pluck('value')->implode('/') }}
                                            </td>
                                            <td class="align-middle">{{ $item->created_at->format('H:i d.m.Y') }}</td>
                                            <td class="d-flex justify-content-start align-middle">
                                                {!! Form::open(['route' => ['admin.tours.destroy', $item->id], 'method' => 'DELETE']) !!}
                                                <button class="btn btn-danger">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                        </tbody>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
