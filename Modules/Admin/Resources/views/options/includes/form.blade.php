

<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab"
           aria-controls="nav-home" aria-selected="true">
            Описание
        </a>
        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab"
           aria-controls="nav-profile" aria-selected="false">
            Цены
        </a>
        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-tour" role="tab"
           aria-controls="nav-profile" aria-selected="false">
            Выбрать нужный тур
        </a>
    </div>
</nav>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="w-100">
                        Ссылка на изображение:
                        @include('admin::media.includes.modal', ['localName' => 'image', 'src' => optional($item->media)->src?:"/not-found-photo.jpg"])
                        {!! Form::hidden('image', null, ['class' => 'form-control']) !!}
                    </label>
                </div>
            </div>
        </div>
        @foreach($item->dataWithLocale as $data)
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="w-100">
                            {{ $data->title }}:
                            {!! Form::text('dataWithLocale[title][value]', null, ['class' => 'form-control']) !!}
                        </label>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="w-100">
                        Цена в UAH:
                        {!! Form::number('price[uah]', null, ['class' => 'form-control']) !!}
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="w-100">
                        Цена в USD:
                        {!! Form::number('price[usd]', null, ['class' => 'form-control']) !!}
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="w-100">
                        Цена в EUR:
                        {!! Form::number('price[eur]', null, ['class' => 'form-control']) !!}
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="nav-tour" role="tabpanel" aria-labelledby="nav-profile-tab">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Туры:
                        {!! Form::select('tours[]', $tours_select, null, ['class' => 'form-control', 'multiple']) !!}
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>

@section('scripts')
    @yield('media')
@endsection
