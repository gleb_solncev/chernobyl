@extends('admin::layouts.master')

@section('content')
    <div class="row mb-2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 d-flex justify-content-end">
                            <button class="btn btn-success"
                                    onclick="location.href='{{ route('admin.options.create') }}'">
                                Создать
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Опции</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-hover hover-pointer">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Название</th>
                                    <th>Цены</th>
                                    <th>Действия</th>
                                </tr>
                                </thead>
                                @foreach($list as $item)
                                    <tbody>
                                    <tr onclick='location.href="{{ route('admin.options.edit', $item->id) }}"'>
                                        <td>
                                            <a href="{{ route('admin.options.edit', $item->id) }}">
                                                {{ $item->id }}
                                            </a>
                                        </td>
                                        <td>
                                            <div style="max-width: 100px; max-height: 150px">
                                                <img src="{{ optional($item->media)->src?:"/not-found-photo.jpg" }}" class="w-100 h-auto" style="max-width: 64px;">
                                            </div>
                                        </td>
                                        <td>{{ optional($item->firstByName('title'))->value }}</td>
                                        <td>{{ $item->price->map(function($price){return $price->value."/".$price->currency;})->implode('|') }}</td>
                                        <td class="d-flex justify-content-start">
                                            {!! Form::open(['route' => ['admin.options.destroy', $item->id], 'method' => 'DELETE']) !!}
                                            <button class="btn btn-danger">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    </tbody>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
