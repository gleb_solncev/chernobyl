@extends('admin::layouts.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 d-flex justify-content-end">
                            <button class="btn btn-success"
                                    onclick="location.href='{{ route('admin.souvenirs.create') }}'">
                                Создать
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Сувениры</div>
                <div class="card-body">
                    @if($list->isEmpty())
                        <div class="row">
                            <div class="col-md-12">
                                Здесь пусто....
                            </div>
                        </div>
                    @else
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-hover hover-pointer">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Изображение</th>
                                        <th>Заголовок</th>
                                        <th>Цена</th>
                                        <th>Краткое описание</th>
                                        <th>Действия</th>
                                    </tr>
                                    </thead>
                                    @foreach($list as $item)
                                        <tbody>
                                        <tr onclick='location.href="{{ route('admin.souvenirs.edit', $item->id) }}"'>
                                            <td>
                                                <a href="{{ route('admin.souvenirs.edit', $item->id) }}">
                                                    {{ $item->id }}
                                                </a>
                                            </td>
                                            <td>
                                                <div style="max-width: 100px; max-height: 150px">
                                                    <img src="{{ optional($item->media)->src?:"/not-found-photo.jpg" }}" class="w-100 h-auto" style="max-height: 150px">
                                                </div>
                                            </td>
                                            <td>{{ $item->getField('title')->value }}</td>
                                            <td>{{ $item->priceWithCurrency() }}</td>
                                            <td>
                                                {{ Str::words($item->getField('description')->value, 20) }}
                                            </td>
                                            <td class="d-flex justify-content-start">
                                                {!! Form::open(['route' => ['admin.souvenirs.destroy', $item->id], 'method' => 'DELETE']) !!}
                                                    <button class="btn btn-danger">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                        </tbody>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 d-flex justify-content-center">
                                {{ $list->links() }}
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
