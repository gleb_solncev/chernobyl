<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-content" role="tab"
           aria-controls="nav-home" aria-selected="true">
            Контент
        </a>
        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-price" role="tab"
           aria-controls="nav-profile" aria-selected="false">
            Цена
        </a>
        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-options" role="tab"
           aria-controls="nav-profile" aria-selected="false">
            Опции
        </a>
    </div>
</nav>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-content" role="tabpanel" aria-labelledby="nav-home-tab">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Slug:
                        {{ Form::text('slug', null, ['class' => 'form-control', 'disabled']) }}
                    </label>
                </div>
            </div>
            <div class="col-md-6">
                Изображение:
                @include('admin::media.includes.modal', ['localName' => 'img', 'src' => optional($item->media)->src?:"/not-found-photo.jpg"])
                {{ Form::hidden('img', optional($item->media)->id) }}
            </div>
        </div>
        @foreach($item->data as $num => $data)
            @if($data->field_type == "input")
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="w-100">
                                {{ $data->title }}:
                                {{ Form::text('data['.$data->name.'][value]', null, ['class' => 'form-control']) }}
                            </label>
                        </div>
                    </div>
                </div>
            @elseif($data->field_type == "textarea")
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="w-100">
                                {{ $data->title }}:
                                {{ Form::textarea('data['.$data->name.'][value]', null, ['class' => 'form-control']) }}
                            </label>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
    <div class="tab-pane fade" id="nav-price" role="tabpanel" aria-labelledby="nav-profile-tab">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Валюта:
                        {!! Form::select('price[currency]]', config('app.currencies_label'),null, ['class' => 'form-control']) !!}
                    </label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Цена:
                        {!! Form::number('price[value]', null, ['class' => 'form-control']) !!}
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="nav-options" role="tabpanel" aria-labelledby="nav-profile-tab">
        <div class="repeat">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="w-100">
                            Добавить:
                            <button class="btn btn-primary form-control add-new" type="button">+</button>
                        </label>
                    </div>
                </div>
            </div>
            @foreach($item->options as $num => $option)
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="w-100">
                                Название:
                                {!! Form::text('options['.$num.'][attr_title]', $option->attr_title, ['class' => 'form-control']) !!}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="w-100">
                                Значение:
                                {!! Form::text('options['.$num.'][opti_title]', $option->opti_title, ['class' => 'form-control']) !!}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="w-100">
                                Выбрать:<br>
                                {!! Form::checkbox('options['.$num.'][selection]', null, $option->selection, ['class' => 'form-control']) !!}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-2 parent-btn">
                        <div class="form-group">
                            <label class="w-100">
                                Удалить:
                                <button type="button" class="btn btn-danger form-control rm">X</button>
                            </label>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

@section('scripts')
    @yield('media')

    <script>
        jQuery(function(){
            html_code = '<div class="row">\n' +
                '            <div class="col-md-5">\n' +
                '                <div class="form-group">\n' +
                '                    <label class="w-100">\n' +
                '                        Название:\n' +
                '                        <input name="options[0][attr_title]" value="" class="form-control">\n' +
                '                    </label>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '            <div class="col-md-3">\n' +
                '                <div class="form-group">\n' +
                '                    <label class="w-100">\n' +
                '                        Значение:\n' +
                '                        <input name="options[0][opti_title]" value="" class="form-control">\n' +
                '                    </label>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '            <div class="col-md-2">\n' +
                '                <div class="form-group">\n' +
                '                    <label class="w-100">\n' +
                '                        Выбрать:<br>\n' +
                '                        <input type="checkbox" name="options[0][selection]" class="form-control">\n' +
                '                    </label>\n' +
                '                </div>\n' +
                '            </div>'+
                '            <div class="col-md-2 parent-btn">\n' +
                '                <div class="form-group">\n' +
                '                    <label class="w-100">\n' +
                '                        Удалить:\n' +
                '                        <button type="button" class="btn btn-danger form-control rm">X</button>\n' +
                '                    </label>\n' +
                '                </div>\n' +
                '            </div>'+
                '        </div>';

            $('.add-new').click(function(){
                length = $('.repeat').find('.rm').length;
                $(html_code).clone()
                    .find('input[name="options[0][attr_title]"]').attr('name', 'options['+length+'][attr_title]')
                    .parents('.row')
                    .find('input[name="options[0][opti_title]"]').attr('name', 'options['+length+'][opti_title]')
                    .parents('.row')
                    .find('input[name="options[0][selection]"]').attr('name', 'options['+length+'][selection]')
                    .parents('.row')
                    .appendTo('.repeat');
            });

            $('html').on('click', '.rm', function(){
                $(this).parents('.parent-btn').parent().remove();
            });
        })
    </script>
@endsection
