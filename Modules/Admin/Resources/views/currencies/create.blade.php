@extends('admin::layouts.master')

@section('content')
    <div class="card">
        <div class="card-header">CREATE</div>
        <div class="card-body">

            {!! Form::model($item, ['route' => 'admin.users.store', 'method' => 'POST']) !!}


            <div class="row">
                <div class="col-md-12 d-flex justify-content-end">
                    <button class="btn btn-primary">Добавить</button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @include('admin::user.includes.form')
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>
@endsection
