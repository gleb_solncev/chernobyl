@extends('admin::layouts.master')

@section('content')

    {!! Form::model( $list, ['route' => ['admin.currencies.store'], 'method' => 'POST']) !!}
        <div class="row mb-2">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-end">
                            <button class="btn btn-primary" type="submit">Обновить</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Валюты</div>
                    <div class="card-body">
                        @foreach($list as $num => $currency)
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="w-100">
                                            Валюта: {{ strtoupper($currency->name) }}
                                            Коефициент:
                                            {!! Form::text('coefficient['.$currency->name.']', $currency->coefficient, ['class' => 'form-control']) !!}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

    {!! Form::close() !!}
@endsection
