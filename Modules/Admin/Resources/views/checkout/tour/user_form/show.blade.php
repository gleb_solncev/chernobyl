@extends('admin::layouts.master')

@section('content')

    <div class="row mb-4">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body d-flex justify-content-end">
                    <a href="{{ route('admin.checkout.tours.index') }}" class="btn btn-secondary">
                        Вернуться
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-4">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Пользователь</div>
                <div class="card-body">
                    <ul>
                        <li>
                            ФИО: {{ $item->user->name }}
                        </li>
                        <li>
                            Email: {{ $item->user->email }}
                        </li>
                        <li>
                            Role: {{ $item->user->role }}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Заказ</div>
                <div class="card-body">
                    <ul>
                        <li>
                            Статус: {{ $item->status }}
                        </li>
                        <li>
                            Тип: {{ $item->type }}
                        </li>
                        <li>
                            Выбранная валюта: {{ $item->currency }}
                        </li>
                        <li>
                            Номер телефона: {{ $item->phone }}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Опции</div>
                <div class="card-body">
                    <ul>
                        <li>
                            {{ $item->option->name }}
                        </li>
                        <li>
                            Количество: {{ $item->option->quantity }}
                        </li>
                        <li>
                            Цена: {{ $item->option->price }}
                        </li>
                        <li>
                            Валюта: {{ $item->option->currency }}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Заказ</div>
                <div class="card-body">
                    <ul>
                        <li>
                            Процент оплаты: {{ $item->payment->method }}
                        </li>
                        <li>
                            Система оплаты: {{ $item->payment->system }}
                        </li>
                        <li>
                            Статус: {{ $item->payment->status }}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
