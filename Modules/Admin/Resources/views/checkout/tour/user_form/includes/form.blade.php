<div class="mt-4">
    <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <label class="w-100">
                    Номер квитанции:
                    {{ Form::text('invoice', null, ['class' => 'form-control']) }}
                </label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="w-100">
                    Статус:
                    {{ Form::select('status', ['unconfirmed' => 'Не подтвержден', 'approval' => 'На утверждении', 'confirmed' => 'Уствержден'], null, ['class' => 'form-control']) }}
                </label>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label class="w-100">
                    Фамилия:
                    {{ Form::text('last_name', null, ['class' => 'form-control']) }}
                </label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="w-100">
                    Имя:
                    {{ Form::text('first_name', null, ['class' => 'form-control']) }}
                </label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="w-100">
                    Отчество:
                    {{ Form::text('middle_name', null, ['class' => 'form-control']) }}
                </label>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label class="w-100">
                    Серия и номер паспорта:
                    {{ Form::text('passport', null, ['class' => 'form-control']) }}
                </label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="w-100">
                    Дата рождения:
                    {{ Form::date('birthday', null, ['class' => 'form-control']) }}
                </label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="w-100">
                    Гражданство:
                    {{ Form::text('national', null, ['class' => 'form-control']) }}
                </label>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="w-100">
                    Телефон:
                    {{ Form::text('phone', null, ['class' => 'form-control']) }}
                </label>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="w-100">
                    Почта:
                    {{ Form::text('email', null, ['class' => 'form-control']) }}
                </label>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="w-100">
                    Комментарий:
                    {{ Form::textarea('comment', null, ['class' => 'form-control']) }}
                </label>
            </div>
        </div>
    </div>
</div>
