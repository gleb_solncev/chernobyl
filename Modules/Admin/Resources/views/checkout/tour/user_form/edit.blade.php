@extends('admin::layouts.master')

@section('content')
    <div class="card">
        <div class="card-header">CREATE</div>
        <div class="card-body">

            {!! Form::model($item, ['route' => ['admin.checkout.tours.forms.update', $checkout_id, $item->id], 'method' => 'PATCH']) !!}

            <div class="row">
                <div class="col-md-12 d-flex justify-content-end">
                    <a style="color:white" class="btn btn-secondary mr-2" onclick="location.href='{{ route("admin.checkout.tours.forms.index", $checkout_id) }}'">Вернуться</a>
                    <a style="color:white" href="{{ route('share_link', $item->share_code) }}" class="btn btn-info mr-2" target="_blank">Отдельная форма</a>
                    <button class="btn btn-primary">Обновить</button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @include('admin::checkout.tour.user_form.includes.form')
                </div>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
