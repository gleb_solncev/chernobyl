@extends('admin::layouts.master')

@section('content')
    <div class="card">
        <div class="card-header">Разрешительные формы ({{$list->total()}})</div>
        <div class="card-body">
            <div class="row mb-2">
                <div class="col-md-12 d-flex justify-content-end">
                    <a style="color:white" href="{{ route('admin.checkout.tours.index') }}" class="btn btn-secondary">
                        Вернуться
                    </a>
                    {{--<button class="btn btn-success"
                            onclick="location.href='{{ route('admin.checkout.tours.create') }}'">
                        Создать
                    </button>--}}
                </div>
            </div>
            @if($list)
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-hover hover-pointer">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>ФИО</th>
                                <th>Статус</th>
                                <th>Национальность</th>
                                <th>SHARE CODE</th>
                                <th>Создан</th>
                            </tr>
                            </thead>
                            @foreach($list as $item)
                                <tbody>
                                    <tr onclick='location.href="{{ route('admin.checkout.tours.forms.edit', [$checkout_id, $item->id]) }}"'>
                                        <td>
                                            <a href="{{ route('admin.checkout.tours.forms.edit',[$checkout_id, $item->id]) }}">
                                                {{ $item->id }}
                                            </a>
                                        </td>
                                        <td>{{ implode(' ', [$item->first_name, $item->last_name, $item->middle_name])?:"-" }}</td>
                                        <td>
                                            {{ __('admin.forms.status.'.$item->status) }}
                                        </td>
                                        <td>{{ $item->national?:"-" }}</td>
                                        <td>{{ $item->share_code }}</td>
                                        <td>{{ $item->created_at->format('H:i d.m.Y') }}</td>
                                        <td>
                                            {!! Form::model($item, ['route' => ['admin.checkout.tours.forms.destroy', $checkout_id, $item->id], 'method' => "DELETE"]) !!}
                                                <button type="submit" class="btn btn-danger">CLEAR</button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="d-flex justify-content-center">
                            {{ $list->links() }}
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
