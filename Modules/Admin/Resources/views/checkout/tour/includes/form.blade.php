
<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab"
           aria-controls="nav-home" aria-selected="true">
            Пользователь
        </a>
        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab"
           aria-controls="nav-profile" aria-selected="false">
            Настроки заказа
        </a>
        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab"
           aria-controls="nav-contact" aria-selected="false">
            Опции
        </a>
        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#payment" role="tab"
           aria-controls="nav-contact" aria-selected="false">
            Оплата
        </a>
    </div>
</nav>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        ФИО:
                        {{ Form::text('user[name]', null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Пользователь:
                        {{ Form::select('user[id]', App\User::all()->keyBy('id')->map(function($usr){return $usr->name;}), null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Статус:
                        {{ Form::select('status', ['cancel' => 'Закрыт', 'expect' => 'Ожидается', 'done' => 'Подтвержден'], null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Валюта:
                        {{ Form::select('currency', config('app.currencies_label'), null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Номер телефона:
                        {{ Form::text('phone', null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Email:
                        {{ Form::text('email', null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Выбрать тип тура:
                        {{ Form::select('product[slug]', ['two_day' => 'Двух дневный тур', 'indevidual' => 'Индивидуальный тур'],
                         null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Валюта:
                        {{ Form::select('product[currency]', config('app.currencies_label'), null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Количество Украинцев:
                        {{ Form::text('product[q_ukraine]', null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Количество Иностранцев:
                        {{ Form::text('product[q_other]', null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Дата начала:
                        {{ Form::date('product[date_start]', null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Дата конца:
                        {{ Form::date('product[date_end]', null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Установить цену Украинцам:
                        {{ Form::number('product[price_ukr]', null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Установить цену Иностранцам:
                        {{ Form::number('product[price_oth]', null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">

        @if($item->options->isNotEmpty())
            @foreach($item->options as $option)
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="w-100">
                                Название:
                                <h4>{{ $option->entity->firstByName('title')->value }}</h4>
                                {{ Form::hidden('option['.$option->id.'][name]', $option->name, ['class' => 'form-control']) }}
                                <span class="small">{!! $option->entity->price->map(function($price){ return "<b>".$price->value."</b>".$price->currency;})->implode('') !!}</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="w-100">
                                Количество:
                                {{ Form::text('option['.$option->id.'][quantity]', $option->quantity, ['class' => 'form-control']) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="w-100">
                                Цена:
                                {{ Form::number('option['.$option->id.'][price]', $option->price, ['class' => 'form-control']) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="w-100">
                                Валюта:
                                {{ Form::select('option['.$option->id.'][currency]', config('app.currencies_label'), $option->currency, ['class' => 'form-control']) }}
                            </label>
                        </div>
                    </div>
                </div>
            @endforeach
        @elseif(isset($options) and !empty($options))
            @foreach($options as $option)
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="w-100">
                                Название:
                                <h4>{{ $option->firstByName('title')->value }}</h4>
                                {{ Form::hidden('option['.$option->id.'][name]', $option->name, ['class' => 'form-control']) }}
                                <span class="small">{!! $option->price->map(function($price){ return "<b>".$price->value."</b>".$price->currency;})->implode('') !!}</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="w-100">
                                Количество:
                                {{ Form::text('option['.$option->id.'][quantity]', $option->quantity, ['class' => 'form-control']) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="w-100">
                                Цена:
                                {{ Form::number('option['.$option->id.'][price]', $option->price, ['class' => 'form-control']) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="w-100">
                                Валюта:
                                {{ Form::select('option['.$option->id.'][currency]', config('app.currencies_label'), $option->currency, ['class' => 'form-control']) }}
                            </label>
                        </div>
                    </div>
                </div>
            @endforeach
            @else
            <div class="row">
                <div class="col-md-12 m-2">
                    Опций не выбрано
                </div>
            </div>
        @endif
    </div>
    <div class="tab-pane fade" id="payment" role="tabpanel" aria-labelledby="nav-contact-tab">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="w-100">
                        Часть оплаты:
                        {{ Form::text('payment[method]', null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="w-100">
                        Использование сервиса оплаты:
                        {{ Form::select('payment[system]',['paypal' => 'PayPal', 'wayforpay' => 'WayForPay', 'liqpay' => "LiqPay"], null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="w-100">
                        Статус:
                        {{ Form::select('payment[status]',['fail' => 'Не оплачено\Ошибка', 'done' => 'Оплачен'], null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
        </div>

    </div>
</div>
