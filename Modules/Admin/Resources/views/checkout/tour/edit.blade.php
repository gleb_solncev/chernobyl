@extends('admin::layouts.master')

@section('content')
    <div class="card">
        <div class="card-header">CREATE</div>
        <div class="card-body">

            {!! Form::model($item, ['route' => ['admin.checkout.tours.update', $item->id], 'method' => 'PATCH']) !!}
            {!! Form::hidden('type', 'tour') !!}

            <div class="row">
                <div class="col-md-12 d-flex justify-content-end">
                    <a style="color:white" class="btn btn-secondary mr-2" onclick="location.href='{{ route("admin.checkout.tours.index") }}'">Вернуться</a>
                    <a style="color:white" class="btn btn-info mr-2" onclick="location.href='{{ route("admin.checkout.tours.forms.index", $item->id) }}'">Формы</a>
                    <button class="btn btn-primary">Обновить</button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @include('admin::checkout.tour.includes.form')
                </div>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
