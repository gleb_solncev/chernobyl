@extends('admin::layouts.master')

@section('content')
    <div class="card">
        <div class="card-header">HEADER</div>
        <div class="card-body">
            <div class="row mb-2">
                <div class="col-md-12 d-flex justify-content-end">
                    <button class="btn btn-success"
                        onclick="location.href='{{ route('admin.checkout.souvenirs.create') }}'">
                        Создать
                    </button>
                </div>
            </div>
            @if($list)
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-hover hover-pointer">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>STATUS</th>
                                <th>Создан</th>
                                <th>Действия</th>
                            </tr>
                            </thead>
                            @foreach($list as $item)
                                <tbody>
                                <tr onclick='location.href="{{ route('admin.checkout.souvenirs.edit', $item->id) }}"'>
                                    <td>
                                        <a href="{{ route('admin.checkout.souvenirs.edit', $item->id) }}">
                                            {{ $item->id }}
                                        </a>
                                    </td>
                                    <td>{{ __('admin.order.status.'.$item->status) }}</td>
                                    <td>{{ $item->created_at->format('H:i d.m.Y') }}</td>
                                    <td class="d-flex justify-content-start">
                                        {!! Form::open(['route' => ['admin.checkout.souvenirs.destroy', $item->id], 'method' => 'DELETE']) !!}
                                            <button class="btn btn-danger">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
