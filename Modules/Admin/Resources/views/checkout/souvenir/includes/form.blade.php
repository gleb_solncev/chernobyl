
<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab"
           aria-controls="nav-home" aria-selected="true">
            Пользователь
        </a>
        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab"
           aria-controls="nav-profile" aria-selected="false">
            Настроки заказа
        </a>
        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#payment" role="tab"
           aria-controls="nav-contact" aria-selected="false">
            Оплата
        </a>
    </div>
</nav>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        ФИО:
                        {{ Form::text('user[name]', null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Пользователь:
                        {{ Form::select('user[id]', App\User::all()->keyBy('id')->map(function($usr){return $usr->name;}), null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Статус:
                        {{ Form::select('status', ['-- Выбрать --', 'cancel' => 'Закрыт', 'expect' => 'Ожидается', 'done' => 'Подтвержден'], null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Валюта:
                        {{ Form::select('currency', config('app.currencies_label'), null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Номер телефона:
                        {{ Form::text('phone', null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Email:
                        {{ Form::text('email', null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
        @foreach($item->souvenirs as $num => $data)
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="w-100">
                            Выбрать тип тура:
                            {{ Form::select('souvenirs['.$num.'][slug]', array_merge(['0' => 'Выбрать'], $souvenirs->toArray()), null, ['class' => 'form-control']) }}
                        </label>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="w-100">
                            Валюта:
                            {{ Form::select('souvenirs['.$num.'][currency]', array_merge(['0' => 'Выбрать'], config('app.currencies_label')), null, ['class' => 'form-control']) }}
                        </label>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="w-100">
                            Цена:
                            {{ Form::number('souvenirs['.$num.'][price]', null, ['class' => 'form-control']) }}
                        </label>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="w-100">
                            Количество:
                            {{ Form::number('souvenirs['.$num.'][quantity]', null, ['class' => 'form-control']) }}
                        </label>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="w-100">
                            Опция:
                            {{ Form::text('souvenirs['.$num.'][name_option]', null, ['class' => 'form-control']) }}
                        </label>
                    </div>
                </div>
            </div>
        @endforeach

    </div>
    <div class="tab-pane fade" id="payment" role="tabpanel" aria-labelledby="nav-contact-tab">

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="w-100">
                        Часть оплаты:
                        {{ Form::text('payment[method]', null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="w-100">
                        Использование сервиса оплаты:
                        {{ Form::select('payment[system]',['paypal' => 'PayPal', 'wayforpay' => 'WayForPay', 'liqpay' => "LiqPay"], null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="w-100">
                        Статус:
                        {{ Form::select('payment[status]',['fail' => 'Не оплачено\Ошибка', 'done' => 'Оплачен'], null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
        </div>

    </div>
</div>
