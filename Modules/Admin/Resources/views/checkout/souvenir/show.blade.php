@extends('admin::layouts.master')

@section('content')

    <div class="row mb-4">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body d-flex justify-content-end">
                    <a href="{{ route('admin.checkout.tours.index') }}" class="btn btn-secondary">
                        Вернуться
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-4">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Заказ</div>
                <div class="card-body">
                    <ul>
                        <li>
                            Статус: <span class="text-danger">{{ __('admin.order.status.'.$item->status) }}</span>
                        </li>
                        <li>
                            Выбран: {{ __('admin.order.type.'.$item->type) }}, {{ $item->product->tour->firstByName('title') }}
                        </li>
                        <li>
                            Выбранная валюта: {{ $item->currency }}
                        </li>
                        <li>
                            Номер телефона: {{ $item->phone }}
                        </li>
                        <li>
                            Количество украинцев: {{ $item->product->q_ukraine }} шт.
                            <br>Цена: {{ $item->product->price_ukr }}. Сумма: {{ $item->product->q_ukraine*$item->product->price_ukr }}
                        </li>
                        <li>
                            Количество иностранцев: {{ $item->product->q_other }} шт.
                            <br>Цена: {{ $item->product->price_oth }}. Сумма: {{ $item->product->q_other*$item->product->price_oth }}
                        </li>
                        <li>
                            Итого: {{ ($item->product->q_ukraine*$item->product->price_ukr) + ($item->product->q_other*$item->product->price_oth) }}
                            {{ $item->currency }}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Пользователь</div>
                <div class="card-body">
                    <ul>
                        <li>
                            ФИО: {{ $item->user->name }}
                        </li>
                        <li>
                            Email: {{ $item->user->email }}
                        </li>
                        <li>
                            Role: {{ $item->user->role }}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Опции</div>
                <div class="card-body">
                    @foreach($item->options as $option)
                        <p class="text-center font-weight-bolder">{{ $option->entity->firstByName('title')->value }}</p>
                        <hr>
                        <ul>
                            <li>
                                Количество: {{ $option->quantity }}
                            </li>
                            <li>
                                Цена: {{ $option->price }}
                            </li>
                            <li>
                                Валюта: {{ $option->currency }}
                            </li>
                        </ul>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Форма оплаты</div>
                <div class="card-body">
                    <ul>
                        <li>
                            Процент оплаты: {{ $item->payment->method }}
                        </li>
                        <li>
                            Система оплаты: {{ $item->payment->system }}
                        </li>
                        <li>
                            Статус: <span class="text-danger">{{ __('admin.payment.status.'.$item->payment->status) }}</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
