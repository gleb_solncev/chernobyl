@extends('admin::layouts.master')

@section('content')

    {!! Form::model($item, ['route' => ['admin.users.update', $item->id], 'method' => 'PATCH']) !!}
    {{ Form::hidden('id') }}

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="col-md-12 d-flex justify-content-end">
                        <a style="color:white" class="btn btn-secondary mr-2"
                           onclick="location.href='{{ route("admin.users.index") }}'">Вернуться</a>
                        <button class="btn btn-primary">Обновить</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">CREATE</div>
                <div class="card-body">
                    @include('admin::user.includes.form')
                </div>
            </div>
        </div>
    </div>

    {!! Form::close() !!}
@endsection
