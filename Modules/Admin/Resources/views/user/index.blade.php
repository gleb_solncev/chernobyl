@extends('admin::layouts.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                        <div class="col-md-12 d-flex justify-content-end">
                            <button class="btn btn-success"
                                    onclick="location.href='{{ route('admin.users.create') }}'">
                                Создать
                            </button>
                        </div>
                    </div>
                </div>
        </div>
    </div>

    <div class="row mt-2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Пользователи</div>
                <div class="card-body">
                    @if($list->isEmpty())
                        <div class="row">
                            <div class="col-md-12">
                                Здесь пусто....
                            </div>
                        </div>
                    @else
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-hover hover-pointer">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>ФИО</th>
                                        <th>Роль</th>
                                        <th>Создан</th>
                                        <th>Действия</th>
                                    </tr>
                                    </thead>
                                    @foreach($list as $item)
                                        <tbody>
                                        <tr onclick='location.href="{{ route('admin.users.edit', $item->id) }}"'>
                                            <td>
                                                <a href="{{ route('admin.users.edit', $item->id) }}">
                                                    {{ $item->id }}
                                                </a>
                                            </td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ __('admin.role.'.$item->role) }}</td>
                                            <td>{{ $item->created_at->format('H:i d.m.Y') }}</td>
                                            <td class="d-flex justify-content-start">
                                                {!! Form::open(['route' => ['admin.users.destroy', $item->id], 'method' => 'DELETE']) !!}
                                                    <button class="btn btn-danger">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                        </tbody>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
