
<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab"
           aria-controls="nav-home" aria-selected="true">
            Пользователь
        </a>
        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab"
           aria-controls="nav-profile" aria-selected="false">
            Настроки
        </a>
    </div>
</nav>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        ФИО:
                        {{ Form::text('name', null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
            <div class="col-md-6">
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Почта:
                        {{ Form::text('email', null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Номер телефона:
                        {{ Form::text('phone', null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Роль:
                        {{ Form::select('role', ['disabled' => 'Заблокирован', 'user' => 'Пользователь', 'admin' => 'Администратор'],null, ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="w-100">
                        Сменить пароль:
                        {{ Form::password('password', ['class' => 'form-control']) }}
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>
