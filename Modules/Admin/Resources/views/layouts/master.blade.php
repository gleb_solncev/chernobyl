<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">


    </head>
    <body>
    @include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])

    <div class="container w-75">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-2">
            <a class="navbar-brand" href="/">
                <img src="/logo.png" style="width: 90px;height: auto;">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse d-flex justify-content-between" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="/">Главная</a>
                    </li>

                    @if(Module::find('locale')->isEnabled())
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ config('app.locales_label')[(App\Http\Middleware\LocaleMiddleware::getLocale()?:"ru")] }}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                {!! Menu::get('LocaleNavMenu')->asDiv( ['class' => '']) !!}
                            </div>
                        </li>
                    @endif

                    @if(Module::find('Currency')->isEnabled())
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ config('app.currencies_label')[(session()->get('currency')?:"uah")] }}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                {!! Menu::get('CurrencyNavMeny')->asDiv(['class' => '']) !!}
                            </div>
                        </li>
                    @endif
                </ul>


                <div class="text-white d-flex justify-content-end">
                    @if(Module::find('Locale')->isEnabled())
                        <div class="locale mr-2">
                            {{ strtoupper(App\Http\Middleware\LocaleMiddleware::getLocale()?:"RU") }}
                        </div>
                    @endif
                    @if(Module::find('Currency')->isEnabled())
                        <div class="currency">
                            {{ strtoupper(session()->get('currency')) }}
                        </div>
                    @endif
                </div>
            </div>
        </nav>

        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header text-white bg-dark">Меню</div>
                    <div class="card-body p-0">
                        {!! Menu::get('MyNavBar')->asUl( ['class' => 'list-group'], ['class' => 'list-group']) !!}
                    </div>
                </div>

                <div class="card mt-2">
                    <div class="card-header text-white bg-dark">Заказы</div>
                    <div class="card-body p-0">
                        {!! Menu::get('OrdersMenu')->asUl( ['class' => 'list-group'], ['class' => 'list-group']) !!}
                    </div>
                </div>

            </div>
            <div class="col-md-9">
                @yield('content')
            </div>
        </div>
    </div>

        {{-- Laravel Mix - JS File --}}
        {{-- <script src="{{ mix('js/admin.js') }}"></script> --}}
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    </body>

@yield('scripts')
    <script>
        $(function(){
            $items = $('ul.list-group li a');
            for(i=0;i<$items.length;i++){
                var tag = $($items[i]),
                    parent = tag.parent(),
                    $href = tag.attr('href');
                parent.attr('onclick', 'location.href="'+$href+'"');
            }
        })
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.2/sweetalert2.all.js"></script>
    @if($errors->any())
        @php $text = [] @endphp
        @foreach ($errors->all() as $error)
            @php $text[] = $error @endphp
        @endforeach
        <script>
            swal ( "Ошибка!" ,  "{!! implode($text, '<br>') !!}" ,  "error" )
        </script>
    @endif
</html>

