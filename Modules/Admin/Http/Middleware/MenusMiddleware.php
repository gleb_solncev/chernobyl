<?php

namespace Modules\Admin\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Nwidart\Modules\Facades\Module;

class MenusMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        \Menu::make('MyNavBar', function ($menu) {
            $menu->add('Главная', ['route' => 'admin.home', 'class' => 'list-group-item hover-pointer'])->active('/admin/');

            $menu->add('Модули', ['route' => 'admin.modules.index', 'class' => 'list-group-item hover-pointer'])->active('/admin/modules/*');
            $menu->add('Пользователи', ['route' => 'admin.users.index', 'class' => 'list-group-item hover-pointer'])->active('/admin/users/*');

            if(Module::find('media')->isEnabled())
                $menu->add('Медиа', ['route' => 'admin.media.index', 'class' => 'list-group-item hover-pointer'])->active('/admin/media/*');

            if(Module::find('tour')->isEnabled())
                $menu->add('Туры', ['route' => 'admin.tours.index', 'class' => 'list-group-item hover-pointer'])->active('/admin/tours/*');

            if(Module::find('souvenir')->isEnabled())
                $menu->add('Сувениры', ['route' => 'admin.souvenirs.index', 'class' => 'list-group-item hover-pointer'])->active('/admin/souvenirs/*');

            if(Module::find('option')->isEnabled())
                $menu->add('Опции', ['route' => 'admin.options.index', 'class' => 'list-group-item hover-pointer'])->active('/admin/options/*');


            if(Module::find('currency')->isEnabled())
                $menu->add('Курс валют', ['route' => 'admin.currencies.index', 'class' => 'list-group-item hover-pointer'])->active('/admin/currencies/*');
        });

        \Menu::make('OrdersMenu', function ($menu) {
            $menu->add('Тур', ['route' => 'admin.checkout.tours.index', 'class' => 'list-group-item hover-pointer'])->active('/admin/checkout/tours/*');
            $menu->add('Сувенир', ['route' => 'admin.checkout.souvenirs.index', 'class' => 'list-group-item hover-pointer'])->active('/admin/checkout/souvenirs/*');
        });

        \Menu::make('LocaleNavMenu', function ($menu) {

            if(Module::find('locale')->isEnabled()) {
                $menu->add('Русский', ['route' => ['setlocale', 'ru'], 'class' => 'dropdown-item']);
                $menu->add('Украинский', ['route' => ['setlocale', 'uk'], 'class' => 'dropdown-item']);
                $menu->add('Английский', ['route' => ['setlocale', 'en'], 'class' => 'dropdown-item']);
            }

        });
        \Menu::make('CurrencyNavMeny', function ($menu) {


            if(Module::find('currency')->isEnabled()) {
                $menu->add('UAH', ['route' => ['currency.set', 'UAH'], 'class' => 'dropdown-item']);
                $menu->add('USD', ['route' => ['currency.set', 'USD'], 'class' => 'dropdown-item']);
                $menu->add('EUR', ['route' => ['currency.set', 'EUR'], 'class' => 'dropdown-item']);
            }

        });

        return $next($request);
    }
}
