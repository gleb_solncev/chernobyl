<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Nwidart\Modules\Facades\Module;
use RealRashid\SweetAlert\Facades\Alert;

class ModuleController extends Controller
{
    public function __construct()
    {
        $this->model = Module::scan();
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
       $list = collect(array_keys($this->model))->map(function($item){
            $module = Module::find($item);
            $name = $module->getLowerName();
            return [
                'name' => $name,
                'title' => 'admin.modules.'.$name.'.title',
                'description' => 'admin.modules.'.$name.'.description',
                'module' => $module->getLowerName(),
                'enable' => $module->isEnabled()
            ];
        });

        return view('admin::modules.index',
            compact('list')
        );
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $req = $request->except('_token');
        $name = key($req);
        $value = reset($req);
        $module = Module::find($name);

        if($value == "on") {
            $module->enable();
            Alert::success(__('admin.modules.'.$name.'.title'), 'Активировано!');
        }elseif($value == "off"){
            $module->disable();
            Alert::info(__('admin.modules.'.$name.'.title'), 'Деактивирован!');
        }

        return redirect()->route('admin.modules.index');
    }

}
