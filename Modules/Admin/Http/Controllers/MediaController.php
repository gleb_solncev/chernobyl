<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Modules\Media\Entities\Media;
use Nwidart\Modules\Facades\Module;
use RealRashid\SweetAlert\Facades\Alert;

class MediaController extends BaseController
{
    /**
     * MediaController constructor.
     */
    public function __construct()
    {
        $this->model = new Media;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->checkFiles();
        $media = $this->model->all();

        return view('admin::media.index',
            compact('media')
        );
    }

    protected function checkFiles(){
        $files = Storage::allFiles('public');
        $this->model->all()->map(function($media) use($files){
            if(!in_array($media->public, $files)) $media->delete();
        });

        collect($files)->map(function($path){
            if($path == 'public/.gitignore') return null;
            $empty = $this->model->all()->where('public', $path)->isEmpty();
            iF($empty) unlink(Storage::path($path));
        });
    }


    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $media = $request->file('media');
        $public = $media->store('public');
        $name =  $media->hashName();

        $src = str_replace('public', '/storage', $public);
        $this->model->replicate()
            ->fill(compact('src', 'public', 'name'))
            ->save();

        Alert::success('Медиафайл', "Успешно удален!");
        return redirect()->route('admin.media.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $item = $this->model->find($id);
        $item->delete();
        $this->checkFiles();

        Alert::success('Медиафайл', "Успешно удален!");
        return redirect()->route('admin.media.index');
    }


    /**
     * Get list Media files
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function ajaxLoad(){
        if(Module::find('media')->isEnabled()) {
            $this->checkFiles();
            return \response($this->model->all()->map(function ($image) {
                return [
                    'id' => $image->id,
                    'src' => $image->src
                ];
            })->toArray());
        }else return \responce(['status'=> 403]);
    }
}
