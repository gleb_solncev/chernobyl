<?php

namespace Modules\Admin\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Modules\Admin\Http\Requests\UserRequest;
use RealRashid\SweetAlert\Facades\Alert;

class UserController extends BaseController
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->model = new User;
    }

    /**
     * Display a listing of the resource.
     * * @return \Illuminate\View\View
     */
    public function index()
    {
        $list = $this->model->where('id', '!=', auth()->id())->get();

        return view('admin::user.index',
            compact('list')
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $item = $this->model;

        return view('admin::user.create',
            compact('item')
        );
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserRequest $request)
    {
        $this->model->fill($request->all())->save();

        Alert::success('Пользователь', 'Успешно создан!!');
        return redirect()->route('admin.users.edit', $this->model->id);
    }


    /**
     * Show the form for editing the specified resource.
     * @param User $id
     * @return \Illuminate\View\View
     */
    public function edit(User $user)
    {
        return view('admin::user.edit',[
                'item' => $user
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserRequest $request, $id)
    {
        $item = $this->model->find($id);
        $all = $request->all();
        if(!$all['password']) unset($all['password']);
        else $all['password'] = Hash::make($all['password']);

        $item->fill($all)->save();

        Alert::success('Пользователь', 'Успешно отредаткированно!');
        return redirect()->route('admin.users.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $item = $this->model->find($id);
        $item->role = 'disabled';
        $item->save();

        Alert::info('Пользователь', 'Успешно блокирован!');
        return redirect()->route('admin.users.index');
    }
}
