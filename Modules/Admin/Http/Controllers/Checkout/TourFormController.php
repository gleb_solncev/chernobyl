<?php

namespace Modules\Admin\Http\Controllers\Checkout;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;
use Modules\Admin\Entities\Checkout;
use Modules\Admin\Entities\Checkout\FormUser;
use RealRashid\SweetAlert\Facades\Alert;

class TourFormController extends Controller
{
    public function __construct()
    {
        $this->model = new FormUser;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($checkout_id)
    {
        $list = $this->model->where(compact('checkout_id'))->orderBy('created_at', 'DESC')->paginate(15);

        if($this->refreshQuantity($checkout_id, $list->total()))
            return redirect()->route('admin.checkout.tours.forms.index', $checkout_id);

        return view('admin::checkout.tour.user_form.index',
            compact('list', 'checkout_id')
        );
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($checkout_id, $id)
    {
        $item = $this->model->find($id);

        return view('admin::checkout.tour.user_form.edit',
            compact('checkout_id', 'item')
        );
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $checkout_id, $id)
    {
        $model = $this->model->find($id);
        $model->fill($request->all())->save();

        Alert::success('Разрешительная форма', "Данные успешно изменены!");
        return redirect()->route('admin.checkout.tours.forms.edit', [$checkout_id, $id]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($checkout_id, $id)
    {
        $item = $this->model->find($id);

        collect($this->model->getFillable())->map(function($name) use($item){
            $item->$name = $name=="status"?"unconfirmed":null;
        });

        $item->save();

        Alert::success('Разрешительная форма', "Данные успешно изменены!");
        return redirect()->route('admin.checkout.tours.forms.index', $checkout_id);
    }

    private function refreshQuantity($checkout_id, $total)
    {
        $data = Checkout\Product::where(compact('checkout_id'))->first();
        $size = $data->q_ukraine+$data->q_other;
        $status = false;

        if($size - $total) {
            for ($i = 0; $i < ($size - $total); $i++) {
                $new = new $this->model;
                $new->checkout_id = $checkout_id;
                $new->save();//CREATE
                $new->share_code = base64_encode($new->id . "|" . $new->checkout_id);
                $new->save();//UPDATE
            }
            $status = true;
        }

        return $status;
    }
}
