<?php

namespace Modules\Admin\Http\Controllers\Checkout;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Entities\Checkout;
use Modules\Admin\Http\Controllers\BaseController;
use Modules\Admin\Http\Requests\CheckoutTourRequest;
use Modules\Option\Entities\Option;
use RealRashid\SweetAlert\Facades\Alert;

class TourController extends BaseController
{
    /**
     * TourController constructor.
     */
    public function __construct()
    {
        $this->model = new Checkout;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $list = $this->model->where('type', 'tour')->get()->sortByDesc('created_at');
        return view('admin::checkout.tour.index',
            compact('list')
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $item = $this->model;
        $options = Option::all();

        return view('admin::checkout.tour.create',
            compact('item', 'options')
        );
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CheckoutTourRequest $request)
    {

        $user_id =(int) $request->user['id'];
        $this->model->fill(array_merge($request->all(), compact('user_id')));
        $this->model->save();

        $this->model->product()->create($request->product);

        $this->model->options()->createMany(collect($request->option)->map(function($item){
            return ($item['price'] and $item['quantity'] and $item['name'])?$item:null;
        })->filter()->toArray());

        $this->model->payment()->create($request->payment);

        $this->model->user->fill($request->user);

        $this->createUserForms($this->model, $request->product['q_ukraine'],  $request->product['q_other']);

        Alert::success('Добавление заказа', 'Успешно!');
        return redirect()->route('admin.checkout.tours.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $item = $this->model->find($id);

        return view('admin::checkout.tour.show',
            compact('item')
        );
    }

    /**
     * Show the form for editing the specified resource.
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $item = $this->model->find($id);

        return view('admin::checkout.tour.edit',
            compact('item')
        );
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $item = $this->model->find($id);
        $this->createUserForms($item, $request->product['q_ukraine'],  $request->product['q_other']);

        $item->fill($request->all())->save();

        $item->user->fill($request->user)->save();

        $item->product->fill($request->product)->save();

        $item->options->map(function($option, $Num) use($request){
            $rOption = collect($request->option)->where('name', $option->name)->first();
            if($rOption['quantity'] and $rOption['price'])
                $option->fill($rOption)->save();
            else
                $option->delete();
        });

        $item->payment->fill($request->payment)->save();

        Alert::info('Обновление заказа', 'Успешно!');
        return redirect()->route('admin.checkout.tours.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $item = $this->model->find($id);
        if($item)
        $item->delete();

        Alert::success('Удаление заказа', 'Успешно!');
        return redirect()->back();
    }

    /**
     * Checker quantity user forms to access in Chernobyl
     * @param Checkout $model
     * @param int $quantity_ua
     * @param int $quantity_o
     */
    protected function createUserForms(Checkout $model,int $quantity_ua,int $quantity_o): void
    {
        $countForm =(int) $quantity_ua + $quantity_o;
        if($model->userForm->count() != $countForm) $model->userForm()->delete();

        if($model->userForm->isEmpty())
            for ($i = 0; $i < $countForm; $i++)
                $model->userForm()->create(['status' => 'unconfirmed']);

        # Пересобрал ибо нужно внести код
        $model->userForm()->get()->map(function ($uf) {
            $uf->share_code = base64_encode($uf->id . "|" . $uf->checkout_id);
            $uf->save();
        });
    }

}
