<?php

namespace Modules\Admin\Http\Controllers\Checkout;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Entities\Checkout;
use Modules\Souvenir\Entities\Souvenir;
use RealRashid\SweetAlert\Facades\Alert;

class SouvenirController extends Controller
{
    public function __construct()
    {
        $this->model = new Checkout;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $list = $this->model->where('type', 'souvenirs')
            ->get()->sortByDesc('created_at');

        return view('admin::checkout.souvenir.index',
            compact('list')
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $item = $this->model;

        $souvenirs = Souvenir::all()->keyBy('slug')->map(function($item){
            return $item->getField('title')->value;
        });
        $total_price = 0;
        $item->souvenirs = collect()->add([])->add([])->add([])->add([])->add([]);

        return view('admin::checkout.souvenir.create',
            compact('item', 'souvenirs', 'total_price')
        );
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $user_id = $request->user['id'];
        $item = $this->model->fill(array_merge(compact('user_id'), $request->all()));
        $item->save();

        collect($request->souvenirs)
            ->map(function($s){
                    return ($s['slug'])?$s:null;
            })->filter()
            ->map(function($s) use($item){
                $item->souvenirs()->create($s);
            });

        $item->payment()->create($request->payment);

        Alert::success('Заказ', 'Успешно создан!');
        return redirect()->route('admin.checkout.souvenirs.edit', $item->id);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $item = $this->model->find($id);
        $souvenirs = Souvenir::all()->keyBy('slug')->map(function($item){
            return $item->getField('title')->value;
        });
        $total_price = $item->souvenirs->map(function($item){
            return $item->quantity * $item->price;
        })->sum();

        return view('admin::checkout.souvenir.edit',
            compact('item', 'souvenirs', 'total_price')
        );
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $item = $this->model->find($id);
        $item->fill($request->all())->save();
        if(!$request->payment['method'] or !$item->payment){
            Alert::error('Ошибка', 'Ошибка механизма оплаты!');
            return redirect()->route('admin.checkout.souvenirs.edit', $item->id);
        }
        $item->payment->fill($request->payment)->save();
        $item->user->fill($request->user)->save();

        $res_s = collect($request->souvenirs)->map(function($souvenir) use($item){
            if(!$souvenir['slug']) return null;
            $item->souvenirs->where('slug', $souvenir['slug'])->first()->fill($souvenir)->save();
            return 1;
        })->filter();
        if($res_s->count() == 0){
            Alert::error('Ошибка', 'Нужно выбраьт хоть один товар');
            return redirect()->route('admin.checkout.souvenirs.edit', $item->id);
        }

        Alert::success('Заказ', 'Успешно отредактирован!');
        return redirect()->route('admin.checkout.souvenirs.edit', $item->id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
