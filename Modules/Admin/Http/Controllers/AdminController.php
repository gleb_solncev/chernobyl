<?php

namespace Modules\Admin\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Carbon;
use Modules\Admin\Entities\Checkout;

class AdminController extends Controller
{

    /**
     * @var \Eloquent $model
     */
    protected $model;

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $stat_orders = collect([
            'year' => Checkout::whereYear('created_at', date('Y'))->count(),
            'month' => Checkout::whereMonth('created_at', date('m'))->count(),
            'day' => Checkout::whereDate('created_at', Carbon::today())->count()
        ]);
        $stat_users = collect([
            'year' => User::whereYear('created_at', date('Y'))->count(),
            'month' => User::whereMonth('created_at', date('m'))->count(),
            'day' => User::whereDate('created_at', Carbon::today())->count()
        ]);

        return view('admin::index',
            compact('stat_orders', 'stat_users')
        );
    }
}
