<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Admin\Http\Requests\CurrencyRequest;
use Modules\Currency\Entities\Currency;
use RealRashid\SweetAlert\Facades\Alert;

class CurrencyController extends BaseController
{
    /**
     * CurrencyController constructor.
     */
    public function __construct()
    {
        $this->model = new Currency;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $list = $this->model->all();

        return view('admin::currencies.index',
            compact('list')
        );
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CurrencyRequest $request)
    {
        collect($request->coefficient)->map(function($coefficient, $name){
            $coefficient = str_replace(',', '.', $coefficient);
            $item = $this->model->where(compact('name'))->first();
            $item->fill(compact('coefficient'));
            $item->save();
        });

        Alert::success('Курс валют', "Успешно отредактирован");
        return redirect()->route('admin.currencies.index');
    }
}
