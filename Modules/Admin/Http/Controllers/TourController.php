<?php

namespace Modules\Admin\Http\Controllers;

use App\Http\Middleware\LocaleMiddleware;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Str;
use Modules\Admin\Http\Requests\TourRequest;
use Modules\Tour\Entities\Tour;
use RealRashid\SweetAlert\Facades\Alert;

class TourController extends BaseController
{
    /**
     * TourController constructor.
     */
    public function __construct()
    {
        $this->model = new Tour;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $list = $this->model->all();

        return view('admin::tours.index',
            compact('list')
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $item = $this->model->first();

        $item->data = $item->getDataByLocale()->groupBy('name')->map(function ($item) {
            $data = $item->first();
            $data->value = null;

            return $data;
        });

        $item->price = $item->price->groupBy('human')->map(function ($price) {
            return $price->groupBy('currency')->map(function ($pp) {
                $price = $pp->first();
                $price->value = null;

                return $price;
            });
        });

        $item->date = [$item->date->map(function ($item) {
            $item->start = $item->end = null;
            return $item;
        })->first()];

        return view('admin::tours.create',
            compact('item')
        );
    }

    /**
     * Store a newly created resource in storage.
     * @param TourRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(TourRequest $request)
    {
        $slug = Str::slug($request->data['title']['value']);

        $item = $this->model->firstOrNew(compact('slug'));
        $item->save();
        $tour_id = $item->id;

        $data = collect($request->data)->map(function ($item, $name) use ($tour_id) {
            $value = $item['value'];
            return Tour\Data::where(compact('name'))->get()->map(function ($model) use ($value, $tour_id) {

                $model->value = $value;
                $model->tour_id = $tour_id;
                unset($model->id);

                return $model->toArray();
            });
        })->flatten(1);
        $item->data()->createMany($data);

        $prices = collect($request->price)->map(function ($price, $human) {
            return collect($price)->map(function ($item, $currency) use ($human) {
                $value = $item['value'];
                return compact('value', 'currency', 'human');
            });
        })->flatten(1);
        $item->price()->createMany($prices);

        $dates = collect($request->date)->map(function ($date) {
            if (isset($date['start']) and isset($date['end'])) return $date;
        })->filter()->toArray();
        $item->date()->createMany($dates);

        Alert::success('Тур', 'Успешно создан!');
        return redirect()->route('admin.tours.index');
    }


    /**
     * Show the form for editing the specified resource.
     * @param Tour $tour
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Tour $tour)
    {
        $item = $tour;

        $item->data = $item->getDataByLocale()->groupBy('name')->map(function ($item) {
            return $item->first();
        });

        $item->price = $item->price->groupBy('human')->map(function ($price) {
            return $price->groupBy('currency')->map(function ($pp) {
                return $pp->first();
            });
        });

        return view('admin::tours.edit',
            compact('item')
        );
    }

    /**
     * Update the specified resource in storage.
     * @param TourRequest $request
     * @param Tour $tour
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(TourRequest $request, Tour $tour)
    {
        $item = $tour;

        collect($request->data)->map(function ($field, $name) use ($item) {
            $value = $field['value'];
            $item->data->where('lang', '=', LocaleMiddleware::getLocale()?:LocaleMiddleware::$mainLanguage)->where('name', '=', $name)->first()
                ->fill(compact('value'))->save();
        });

        collect($request->price)->map(function ($field, $human) use ($item) {
            collect($field)->map(function ($field, $currency) use ($human, $item) {
                $price = $item->price->where('currency', $currency)->where('human', $human)->first();
                $price->value = $field['value'];
                $price->save();
            });
        });

        $item->date()->delete();
        $dates = collect($request->date)->map(function ($date) {
            return (isset($date['start']) and isset($date['end']))?$date:null;
        })->filter()->toArray();
        $item->date()->createMany($dates);

        Alert::success('Тур', "Успешно отредактирован");
        return redirect()->route('admin.tours.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param Tour $tour
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Tour $tour)
    {
        $item = $tour;
        $item->delete();

        Alert::info('Тур', "Запись успрешно удалена");
        return redirect()->route('admin.tours.index');
    }
}
