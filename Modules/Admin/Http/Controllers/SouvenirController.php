<?php

namespace Modules\Admin\Http\Controllers;

use App\Http\Middleware\LocaleMiddleware;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Modules\Admin\Http\Requests\SouvenirRequest;
use Modules\Souvenir\Entities\Souvenir;
use RealRashid\SweetAlert\Facades\Alert;

class SouvenirController extends BaseController
{
    /**
     * SouvenirController constructor.
     */
    public function __construct()
    {
        $this->model = new Souvenir;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $list = $this->model->paginate(15);

        return view('admin::souvenirs.index',
            compact('list')
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $item = $this->getDemoData();

        return view('admin::souvenirs.create',
            compact('item')
        );
    }

    /**
     * @return Souvenir
     */
    private function getDemoData()
    {
        $item = $this->model->first();
        $collection_data = $item->fieldsByLocale();
        $item->img = $item->slug = null;
        $data = $collection_data->map(function ($item) {
            $item->value = $item->souvenir_id = null;
            return $item->replicate();
        })->keyBy('name');

        $demo = $item->replicate();
        $demo->data = $data;
        $demo->options = collect();

        return $demo;
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SouvenirRequest $request)
    {
        $all = $request->all();
        $all['slug'] = Str::slug($request->data['title']['value']);
        $item = $this->model->fill($all);
        $item->save();

        $options = collect(optional($all)['options'])->map(function($option){
            $selection = 0;
            if(isset($option['selection']))
                $selection = 1;
            $option['attr_name'] = Str::slug($option['attr_title']);
            $option['opti_name'] = Str::slug($option['opti_title']);
            return array_merge($option, compact('selection'));
        })->toArray();
        $item->options()->createMany($options);

        $price = $all['price'];
        $item->price()->create($price);

        $data = collect($all['data'])->map(function($item, $name){
            if(!isset($item['value'])) return null;
            $collection = [];
            foreach(config('app.locales') as $lang) {
                $demoData = $this->model->first()->getField($name, $lang);
                $demoData->value = $item['value'];
                $demoData->lang = $lang;
                $demoData->souvenir_id = null;
                $collection[] = $demoData;
            }
            return $collection;
        })->flatten()->toArray();

        $item->data()->createMany($data);

        Alert::success('Сувенир', 'Успешно создан!');
        return redirect()->route('admin.souvenirs.edit', $item->id);
    }

    /**
     * Show the form for editing the specified resource.
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $item = $this->model->find($id);
        $item->data = $item->data->where('lang', LocaleMiddleware::getLocale()?:LocaleMiddleware::$mainLanguage)->keyBy('name');

        return view('admin::souvenirs.edit',
            compact('item')
        );
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SouvenirRequest $request, $id)
    {
        $item = $this->model->find($id)->fill($request->all());
        $item->save();

        collect($request->data)->map(function($data, $name) use($item){
            $item->data->where('name', $name)->where('lang', LocaleMiddleware::getLocale()?:LocaleMiddleware::$mainLanguage)->first()->fill($data)->save();
        });

        $item->price->fill($request->price)->save();

        $item->options()->delete();
        $options = collect($request->options)->map(function($option){
            $selection = 0;
            if(isset($option['selection']))
                $selection = 1;
            $option['attr_name'] = Str::slug($option['attr_title']);
            $option['opti_name'] = Str::slug($option['opti_title']);
            return array_merge($option, compact('selection'));
        })->toArray();
        $item->options()->createMany($options);

        Alert::success('Сувенир', 'Успешно отредактирован!');
        return redirect()->route('admin.souvenirs.edit', $item->id);
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $this->model->find($id)->delete();

        Alert::info('Сувенир', 'Успешно удален!');
        return redirect()->route('admin.souvenirs.index');
    }
}
