<?php

namespace Modules\Admin\Http\Controllers;

use App\Http\Middleware\LocaleMiddleware;
use Illuminate\Support\Str;
use Modules\Admin\Http\Requests\OptionRequest;
use Modules\Option\Entities\Option;
use Modules\Tour\Entities\Tour;
use RealRashid\SweetAlert\Facades\Alert;

class OptionController extends BaseController
{
    /**
     * OptionController constructor.
     */
    public function __construct()
    {
        $this->model = new Option;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $list = $this->model->all();

        return view('admin::options.index',
            compact('list')
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $item = $this->model; # Model

        $tours_select = Tour::all()->keyBy('id')->map(function(Tour $t){
            return $t->firstByName('title');
        })->toArray();# List TOURS

        $item->dataWithLocale = $item->first()->dataWithLocale->map(function($item){
            $new = $item->replicate();
            $new->value = null;
            return $new;
        }); # LIST DATA FIELDS


        return view('admin::options.create',
            compact('item', 'tours_select')
        );
    }

    /**
     * Store a newly created resource in storage.
     * @param OptionRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(OptionRequest $request)
    {
        # insert to DATA
        $name = Str::slug($request->dataWithLocale['title']['value']);
        $image = $request->image;
        $this->model->fill(compact('name', 'image'))->save();

        # Insert Prices
        $prices = collect($request->price)->map(function($value, $currency){
            return compact('value', 'currency');
        })->toArray();
        $this->model->price()->createMany($prices);

        collect($request->dataWithLocale)->map(function($field, $name){
            foreach(config('app.locales') as $lang):
                $en = $this->model->first()->firstByName($name)->replicate();
                $en->value = $field['value'];
                $en->option_id = $this->model->id;
                $en->lang = $lang;
                $en->save();
            endforeach;
        });

        $this->rettach($this->model, $request->tours);

        Alert::success('Опции', "Успешно добавлена");
        return redirect()->route('admin.options.index');

    }

    /**
     * Show the form for editing the specified resource.
     * @param Option $option
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Option $option)
    {
//        dd(LocaleMiddleware::getLocale());
        $item = $option;

        $tours_select = Tour::all()->keyBy('id')->map(function(Tour $t){
            return $t->firstByName('title');
        })->toArray();
        $item->price = $item->price->keyBy('currency')->map(function($price){ return $price->value; });
        $item->dataWithLocale = $item->dataWithLocale->keyBy('name');

        return view('admin::options.edit',
            compact('item', 'tours_select')
        );
    }


    /**
     * Update the specified resource in storage.
     * @param OptionRequest $request
     * @param Option $option
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(OptionRequest $request,Option $option)
    {
        if(!$option->name)
            $request->merge(['name'=>
                Str::slug($request->dataWithLocale['title']['value'])
            ]);

        $option->fill($request->all())->save();

        collect($request->dataWithLocale)->map(function($field, $name) use($option){
            $option->firstByName($name)->fill($field)->save();
        });

        collect($request->price)->map(function($value, $currency) use($option){
            $item = $option->price->where('currency', $currency)->first();
            $item->fill(compact('value'))->save();
        });

        $this->rettach($option, $request->tours);

        Alert::success('Обновлено','Опции успешно обновлены');
        return redirect()->route('admin.options.index');
    }

    protected function rettach(Option $item, $tour_ids){
        $item->tours()->detach();

        collect($tour_ids)->map(function(int $id) use($item){
            $item->tours()->attach($id);
        });
    }

    /**
     * Remove the specified resource from storage.
     * @param Option $option
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Option $option)
    {
        $option->tours()->detach();
        $option->data()->delete();
        $option->delete();

        Alert::info('Опции','Опция успешно удалена');
        return redirect()->route('admin.options.index');
    }
}
