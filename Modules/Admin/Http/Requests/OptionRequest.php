<?php

namespace Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OptionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dataWithLocale.title.value' => 'required',
            'price.uah' => 'required',
            'price.usd' => 'required',
            'price.eur' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'dataWithLocale.title.value.required' => 'Заголовок пуст. Заполните его!',
            'price.uah.required' => 'Укажите цену в гривне',
            'price.usd.required' => 'Укажите цену в долларе',
            'price.eur.required' => 'Укажите цену в евро'
        ];
    }
}
