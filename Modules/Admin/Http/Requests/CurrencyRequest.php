<?php

namespace Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CurrencyRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'coefficient.uah' => 'required',
            'coefficient.usd' => 'required',
            'coefficient.eur' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'coefficient.uah.required' => 'Пожалуйста заполните коефициент для гривны!',
            'coefficient.usd.required' => 'Пожалуйста заполните коефициент для доллара!',
            'coefficient.eur.required' => 'Пожалуйста заполните коефициент для евро!',

        ];
    }
}
