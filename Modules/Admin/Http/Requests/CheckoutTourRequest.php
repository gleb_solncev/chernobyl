<?php

namespace Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CheckoutTourRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required',
            'email' => 'required',
            'status' => 'required',

            'product.q_ukraine' => 'required|integer',
            'product.q_other' => 'required|integer',
            'product.date_start' => 'required',
            'product.date_end' => 'required',
            'product.price_ukr' => 'required|integer',
            'product.price_oth' => 'required|integer',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'phone.required' => 'Нужно заполнить номер телефона!',
            'email.required' => 'Нужно заполнить почту!',
            'status.required' => 'Нужно выбрать статус заказа!',

            'product.q_ukraine.required' => 'Нужно указать количество Украинцев',
            'product.q_ukraine.integer' => 'Поле Количество Украинцев должно быть Числовое',

            'product.q_other.required' => 'Нужно указать коилчество Иностранцев',
            'product.q_other.integer' => 'Поле Количество Иностранцев должно быть Числовое',
            'product.date_start.required' => 'Нужно указать дату старта',
            'product.date_end.required' => 'Нужно указать дату конца',
            'product.price_ukr.required' => 'Нужно указать цену для Украинцев',
            'product.price_ukr.int' => 'Поле Цена для Украинцев должна быть Числовым',

            'product.price_oth.required' => 'Нужно указать цену для Иностранцев',
            'product.price_oth.int' => 'Поле Цена для Иностранцев должна быть Числовым',


        ];
    }
}
