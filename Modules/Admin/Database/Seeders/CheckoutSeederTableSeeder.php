<?php

namespace Modules\Admin\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Modules\Admin\Entities\Checkout;

class CheckoutSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('checkouts')->insert([
            [
                'status' => 'expect',
                'type' => 'tour',
                'currency' => 'usd',
                'phone' => '777',
                'email' => 'admin@admin.com',
                'user_id' => 1,
                'created_at' => now(),
            ],
            [
                'status' => 'expect',
                'type' => 'souvenir',
                'currency' => 'usd',
                'phone' => '30999302',
                'email' => 'admin@admin.com',
                'user_id' => 1,
                'created_at' => now(),
            ],
        ]);


        DB::table('checkout_product')->insert([
            [
                'slug' => 'two_day',
                'q_ukraine' => 1,
                'q_other' => 2,
                'price_ukr' => 100,
                'price_oth' => 150,
                'currency' => 'usd',
                'date_start' => '2020-02-28',
                'date_end' => "2020-03-01",
                'checkout_id' => 1,
            ]
        ]);

        DB::table('checkout_option')->insert([
            [
                'name' => 'dozimetr',
                'quantity' => 2,
                'price' => 50,
                'currency' => 'usd',
                'checkout_id' => 1
            ]
        ]);

        DB::table('checkout_payment')->insert([
            [
                'method' => '30',
                'system' => 'liqpay',
                'status' => 'done',
                'checkout_id' => 1
            ]
        ]);
        DB::table('checkout_form_users')->insert([
            [
                'id' => 1,
                'share_code' => base64_encode("1|1"),//id,checkout_id
                'checkout_id' => 1,
                'created_at' => now()
            ]
        ]);







    }
}
