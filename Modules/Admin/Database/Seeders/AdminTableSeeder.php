<?php

namespace Modules\Admin\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
           [
               'name' => 'Administration Admin Adminovich',
               'email' => 'admin@admin.com',
               'password' => Hash::make('password'),
               'role' => 'admin',
               'created_at' => now()
           ]
        ]);
    }
}
