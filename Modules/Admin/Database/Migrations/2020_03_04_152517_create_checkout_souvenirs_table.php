<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckoutSouvenirsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkout_souvenirs', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('slug');
            $table->string('quantity');
            $table->string('currency');
            $table->float('price');
            $table->string('name_option')->nullable();

            $table->unsignedBigInteger('checkout_id')->nullable();
            $table->foreign('checkout_id')->references('id')->on('checkouts')->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checkout_souvenirs');
    }
}
