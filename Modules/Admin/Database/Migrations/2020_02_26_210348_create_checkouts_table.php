<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkouts', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->enum('status', ['cancel', 'expect', 'done'])->default('expect');
            $table->enum('type', ['none', 'tour', 'souvinirs'])->default('none');
            $table->enum('currency', config('app.currencies'))->default('ru');

            $table->string('phone', 10)->nullable();
            $table->string('email', 50)->nullable();


            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checkouts');
    }
}
