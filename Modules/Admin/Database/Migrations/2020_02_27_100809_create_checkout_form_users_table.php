<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckoutFormUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkout_form_users', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->enum('status', ['unconfirmed', 'approval', 'confirmed'])->default('unconfirmed');

            $table->string('invoice')->nullable();;
            $table->string('first_name')->nullable();;
            $table->string('last_name')->nullable();;
            $table->string('middle_name')->nullable();;
            $table->string('passport')->nullable();
            $table->date('birthday')->nullable();;
            $table->string('national')->nullable();;
            $table->string('phone')->nullable();;
            $table->text('comment')->nullable();;

            $table->string('share_code')->nullable();//base64(id|checkout_id)

            $table->unsignedBigInteger('checkout_id')->nullable();
            $table->foreign('checkout_id')->references('id')->on('checkouts')->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checkout_form_users');
    }
}
