<?php

use Illuminate\Support\Facades\Route;
use Modules\Admin\Http\Middleware\AdministrationMiddleware;


Route::group(['prefix' => App\Http\Middleware\LocaleMiddleware::getLocale()], function() {
    Route::prefix('admin')
        ->as('admin.')
        ->middleware(AdministrationMiddleware::class)
        ->group(function () {
            Route::get('/', 'AdminController@index')->name('home');

            Route::resource('tours', 'TourController')->names('tours');

            Route::prefix('checkout')->as('checkout.')
                ->namespace('Checkout')->group(function () {
                    Route::resource('tours', 'TourController')->names('tours');
                    Route::resource('tours.forms', 'TourFormController')
                        ->except(['create', 'store', 'show'])->names('tours.forms');

                    Route::resource('souvenirs', 'SouvenirController')
                        ->except(['show'])->names('souvenirs');
                });

            Route::resource('users', 'UserController')->except(['show'])->names('users');

            Route::resource('options', 'OptionController')
                ->except(['show'])->names('options');

            Route::resource('module', 'ModuleController')
                ->only(['index', 'store'])->names('modules');

            Route::resource('media', 'MediaController')
                ->except(['show', 'update', 'create', 'edit'])->names('media');
            Route::post('ajax-media', 'MediaController@ajaxLoad')->name('ajax-media');

            Route::resource('souvenirs', 'SouvenirController')
                ->names('souvenirs');

            Route::resource('currencies', 'CurrencyController')
                ->only(['index', 'store'])
                ->names('currencies');
        });
});
