<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    protected $table = 'checkouts';

    protected $fillable = [
        'status', 'type', 'currency',
        'phone', 'email',  'user_id',
    ];

    public function product(){
        return $this->hasOne('Modules\Admin\Entities\Checkout\Product', 'checkout_id', 'id');
    }
    public function options(){
        return $this->hasMany('Modules\Admin\Entities\Checkout\Option', 'checkout_id', 'id');
    }
    public function souvenirs(){
        return $this->hasMany('Modules\Admin\Entities\Checkout\Souvenir', 'checkout_id', 'id');
    }
    public function payment(){
        return $this->hasOne('Modules\Admin\Entities\Checkout\Payment', 'checkout_id', 'id');
    }
    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    public function userForm(){
        return $this->hasMany('Modules\Admin\Entities\Checkout\FormUser', 'checkout_id', 'id');
    }
}
