<?php

namespace Modules\Admin\Entities\Checkout;

use Illuminate\Database\Eloquent\Model;

class Souvenir extends Model
{
    protected $fillable = ['slug', 'quantity', 'price', 'currency', 'name_option'];
    protected $table = 'checkout_souvenirs';
}
