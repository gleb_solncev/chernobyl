<?php

namespace Modules\Admin\Entities\Checkout;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $timestamps = false;

    protected $table = 'checkout_product';
    protected $fillable = ["slug","q_ukraine","q_other","price_ukr","price_oth","currency","date_start","date_end", 'checkout_id'];

    public function tour(){
        return $this->hasOne('Modules\Tour\Entities\Tour', 'slug', 'slug');
    }

}
