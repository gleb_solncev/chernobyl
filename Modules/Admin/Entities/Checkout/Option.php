<?php

namespace Modules\Admin\Entities\Checkout;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    public $timestamps = false;
    protected $table = 'checkout_option';
    protected $fillable = ["name", "quantity", "price", "currency"];

    public function entity(){
        return $this->hasOne('Modules\Option\Entities\Option', 'name', 'name');
    }
}
