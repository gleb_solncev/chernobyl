<?php

namespace Modules\Admin\Entities\Checkout;

use Illuminate\Database\Eloquent\Model;

class FormUser extends Model
{
    protected $fillable = ['invoice', 'status', 'first_name', 'last_name', 'middle_name', 'passport', 'birthday', 'national', 'phone', 'comment'];
    protected $table = 'checkout_form_users';
}
