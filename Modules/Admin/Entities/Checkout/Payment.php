<?php

namespace Modules\Admin\Entities\Checkout;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public $timestamps = false;
    protected $table = 'checkout_payment';
    protected $fillable = ["method","system","status"];
}
