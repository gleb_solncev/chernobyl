<?php

namespace Modules\Currency\Entities;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $fillable = ['coefficient', 'name'];
    protected $table = 'currencies';
}
