<?php

namespace Modules\Currency\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Session;

class CurrencyController extends Controller
{
    /**
     * @param $currency
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setCurrency($currency){
        $currency = strtolower($currency);
        Session::put(compact('currency'));
        Session::save();

        return redirect()->back();
    }

    /**
     * @return string
     */
    public static function getCurrency(): string
    {
        $currency = Session::get('currency');//?:"uah"
        if(!$currency) {
            Session::put('currency', 'uah');
            Session::save();
        }

        return Session::get('currency');
    }

}
