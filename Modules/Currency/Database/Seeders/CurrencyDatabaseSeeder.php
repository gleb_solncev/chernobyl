<?php

namespace Modules\Currency\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CurrencyDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('currencies')->insert([
            [
                'coefficient' => 1,
                'name' => 'uah'
            ],
            [
                'coefficient' => 0.45,
                'name' => 'usd'
            ],
            [
                'coefficient' => 0.38,
                'name' => 'eur'
            ],
        ]);
        // $this->call("OthersTableSeeder");
    }
}
