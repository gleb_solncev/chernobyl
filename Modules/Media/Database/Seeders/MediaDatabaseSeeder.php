<?php

namespace Modules\Media\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MediaDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('media')
            ->insert([
                [
                    'id' => 1,
                    'src' => '/public/17.png',
                    'name' => '17.png',
                    'public' => '/storage/17.png'
                ]
            ]);
    }
}
