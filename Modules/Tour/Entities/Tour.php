<?php

namespace Modules\Tour\Entities;

use App\Http\Middleware\LocaleMiddleware;
use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    protected $table = 'tours';
    protected $fillable = ['slug'];

    public function data(){
        return $this->hasMany('Modules\Tour\Entities\Tour\Data', 'tour_id', 'id');
    }

///////
    public function getDataByLocale($locale=null){
        return $this->hasMany('Modules\Tour\Entities\Tour\Data', 'tour_id', 'id')
            ->where('lang', $locale?:(LocaleMiddleware::getLocale()?:LocaleMiddleware::$mainLanguage))->get();
    }

    public function firstByName($name, $locale=null){
        return $this->hasMany('Modules\Tour\Entities\Tour\Data', 'tour_id', 'id')
            ->where('lang', $locale?:(LocaleMiddleware::getLocale()?:LocaleMiddleware::$mainLanguage))
            ->where('name', $name)->first()->value;
    }
    public function firstDataByNameLocale($name, $locale=null){
        return $this->hasMany('Modules\Tour\Entities\Tour\Data', 'tour_id', 'id')
            ->where('lang', $locale?:(LocaleMiddleware::getLocale()?:LocaleMiddleware::$mainLanguage))
            ->where('name', $name)->first();
    }

///////
    public function price(){
        return $this->hasMany('Modules\Tour\Entities\Tour\Price', 'tour_id', 'id');
    }

    public function getPriceByCurrency($currency=null){
        return $this->hasMany('Modules\Tour\Entities\Tour\Price', 'tour_id', 'id')
            ->where('currency', $currency?:'uah')->get();
    }

///////
    public function date(){
        return $this->hasMany('Modules\Tour\Entities\Tour\Date', 'tour_id', 'id');
    }
}
