<?php

namespace Modules\Tour\Entities\Tour;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $table = 'tour_price';
    protected $fillable = ['human', 'currency', 'value'];
}
