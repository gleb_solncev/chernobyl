<?php

namespace Modules\Tour\Entities\Tour;

use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    protected $table = 'tour_data';
    protected $fillable = ['lang', 'title', 'name', 'value', 'type_field'];

    public function media(){
        return $this->belongsTo('Modules\Media\Entities\Media', 'value', 'id');
    }
}
