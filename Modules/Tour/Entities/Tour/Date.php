<?php

namespace Modules\Tour\Entities\Tour;

use Illuminate\Database\Eloquent\Model;

class Date extends Model
{
    protected $fillable = ['start', 'end'];
    protected $table = 'tour_date';
}
