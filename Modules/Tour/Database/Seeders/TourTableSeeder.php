<?php

namespace Modules\Tour\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TourTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('tours')->insert([
            'id' => 1,
            'slug' => 'two_day',
            'created_at' => now()
        ]);


        DB::table('tour_data')->insert([
            [
                'id' => 1,
                'lang' => 'ru',
                'title' => 'Заголовок страницы',
                'value' => 'Двухдневный тур',
                'name' => 'title',
                'type_field' => 'input',
                'tour_id' => 1,
                'created_at' => now()
            ],
            [
                'id' => 2,
                'lang' => 'en',
                'title' => 'Заголовок страницы',
                'value' => 'Two days tour',
                'name' => 'title',
                'type_field' => 'input',
                'tour_id' => 1,
                'created_at' => now()
            ],
            [
                'id' => 3,
                'lang' => 'uk',
                'title' => 'Заголовок страницы',
                'value' => 'Двухдневный тур',
                'name' => 'title',
                'type_field' => 'input',
                'tour_id' => 1,
                'created_at' => now()
            ],
            ////
            [
                'id' => 4,
                'lang' => 'ru',
                'title' => 'Изображение',
                'value' => 1,
                'name' => 'thumbnail',
                'type_field' => 'media',
                'tour_id' => 1,
                'created_at' => now()
            ],
            [
                'id' => 5,
                'lang' => 'en',
                'title' => 'Изображение',
                'value' => 1,
                'name' => 'thumbnail',
                'type_field' => 'media',
                'tour_id' => 1,
                'created_at' => now()
            ],
            [
                'id' => 6,
                'lang' => 'uk',
                'title' => 'Изображение',
                'value' => 1,
                'name' => 'thumbnail',
                'type_field' => 'media',
                'tour_id' => 1,
                'created_at' => now()
            ],
        ]);


        DB::table('tour_price')->insert([
            [
                'id' => 1,
                'human' => 'ukraine',
                'currency' => 'uah',
                'value' => '1250',
                'tour_id' => 1,
                'created_at' => now()
            ],
            [
                'id' => 2,
                'human' => 'other',
                'currency' => 'uah',
                'value' => '1500',
                'tour_id' => 1,
                'created_at' => now()
            ],
            [
                'id' => 3,
                'human' => 'ukraine',
                'currency' => 'usd',
                'value' => '100',
                'tour_id' => 1,
                'created_at' => now()
            ],
            [
                'id' => 4,
                'human' => 'other',
                'currency' => 'usd',
                'value' => '120',
                'tour_id' => 1,
                'created_at' => now()
            ],
            [
                'id' => 5,
                'human' => 'ukraine',
                'currency' => 'eur',
                'value' => '80',
                'tour_id' => 1,
                'created_at' => now()
            ],
            [
                'id' => 6,
                'human' => 'other',
                'currency' => 'eur',
                'value' => '90',
                'tour_id' => 1,
                'created_at' => now()
            ],
        ]);
        DB::table('tour_date')->insert([
            [
                'id' => 1,
                'start' => '2020-03-05',
                'end' => '2020-03-06',
                'tour_id' => 1,
                'created_at' => now()
            ],
            [
                'id' => 2,
                'start' => '2020-03-10',
                'end' => '2020-03-11',
                'tour_id' => 1,
                'created_at' => now()
            ],
            [
                'id' => 3,
                'start' => '2020-03-13',
                'end' => '2020-03-14',
                'tour_id' => 1,
                'created_at' => now()
            ],
            [
                'id' => 4,
                'start' => '2020-03-20',
                'end' => '2020-03-21',
                'tour_id' => 1,
                'created_at' => now()
            ],
            [
                'id' => 5,
                'start' => '2020-03-25',
                'end' => '2020-03-26',
                'tour_id' => 1,
                'created_at' => now()
            ],
        ]);
    }
}
